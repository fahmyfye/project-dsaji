<?php

namespace App\Http\Middleware;

use App\Http\Models\Restaurant;
use App\Http\Models\RestaurantStaff;
use Closure;
use Auth;
use Arr;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if(!empty(Auth::user()->role)) {
                if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin' || Auth::user()->role == 'restaurant') {
                    // Set Config For Client
                    if(auth()->user()->role == 'restaurant') {
                        $menu = RestaurantStaff::select('restaurant_id', 'role')->where('user_id', auth()->id())->with('restaurants')->get();
                        config([
                            'restaurant.menu' => $menu,
                        ]);
                    }

                    return $next($request);

                } else {
                    return redirect()->route('error');
                }

            } else {
                return redirect()->route('login');
            }
        } else {
            return redirect()->route('login');
        }
    }
}
