<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use Str;
use Hash;
use Validator;
use Auth;

class CustomerController extends Controller
{

   /**
    * Create a new user instance after a valid registration.
    */
   public function register(Request $request) {
   	// Validate Rules
   	$rules = [
         'name'     => 'required|string|max:255',
         'email'    => 'required|email|max:255|unique:customers',
         'password' => 'required|min:4',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

   	if($validator->fails()) {
   		return response()->json(['errors' => $validator->errors()], 400);

   	} else {
   		$data = Customer::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'api_token' => Str::random(60),
            'phone'     => isset($request->phone) ? $request->phone : null,
            'city'      => isset($request->city) ? $request->city : null,
            'state'     => isset($request->state) ? $request->state : null,
            'avatar'    => isset($request->avatar) ? $request->avatar : null,
            'type'      => isset($request->type) ? $request->type : null,
            'type_id'   => isset($request->type_id) ? $request->type_id : null,
   		]);

   		if($data) {
   			return response()->json(['success' => $data], 200);

   		} else {
   			return response()->json(['error' => 'Fail to register'], 400);

   		}
   	}
   }

   public function login(Request $request) {
   	$auth = Customer::where('email', $request->email)->first();

   	if($auth) {
   		$hashed = Hash::check($request->password, $auth->password);
   		if($hashed) {
   			if($auth->api_token == null) {
   				$auth->api_token = Str::random(60);
   				$auth->save();
   			}

   			return response()->json(['success' => $auth], 200); 
   			
   		} else {
   			return response()->json(['error' => 'Unauthorised'], 401);
   		}

   	} else {
   		return response()->json(['error' => 'Unregistered User'], 401);
   	}
   }

   public function logout(Request $request) {
      $data = Customer::where('api_token', $request->header('Authorization'))->first();
      $data->api_token = null;
      $data->save();

      if($data) {
         return response()->json(['success' => 'User logged out'], 200);

      } else {
         return response()->json(['error' => 'Fail to register'], 400);

      }
   }

   public function info(Request $request) {
      $data = Customer::where('api_token', $request->header('Authorization'))->first();

      if($data) {
         return response()->json(['success' => $data], 200);

      } else {
         return response()->json(['error' => 'Customer does not exist'], 404);

      }
   }

   public function update(Request $request) {
      // Validate Rules
      $rules = [
         'name' => 'required|string|max:255',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['errors' => $validator->errors()], 400);

      } else {
         $data = Customer::where('api_token', $request->header('Authorization'))->update([
            'name'      => $request->name,
            'phone'     => isset($request->phone) ? $request->phone : null,
            'city'      => isset($request->city) ? $request->city : null,
            'state'     => isset($request->state) ? $request->state : null,
            'avatar'    => isset($request->avatar) ? $request->avatar : null,
            'type'      => isset($request->type) ? $request->type : null,
            'type_id'   => isset($request->type_id) ? $request->type_id : null,
         ]);

         if($data) {
            $data = Customer::where('api_token', $request->header('Authorization'))->first();
            return response()->json(['success' => $data], 200);

         } else {
            return response()->json(['error' => 'Fail to update user info'], 400);

         }
      }
   }

   protected function password(Request $request) {
      // Validate Rules
      $rules = [
         'password' => 'required|min:4',
      ];

      // Validate Request
      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()) {
         return response()->json(['errors' => $validator->errors()], 400);

      } else {
         $data = Customer::where('api_token', $request->header('Authorization'))->first();
         $data->password = Hash::make($request->password);
         $data->save();

         if($data) {
            return response()->json(['success' => 'Password updated'], 200);

         } else {
            return response()->json(['error' => 'Fail to update password'], 400);

         }
      }
   }
}
