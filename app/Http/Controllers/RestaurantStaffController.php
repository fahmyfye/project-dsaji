<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Role;
use App\Http\Models\Restaurant;
use App\Http\Models\RestaurantStaff;
use Auth;
use Hash;

class RestaurantStaffController extends Controller
{
	// const ROLES = 
	// [
 //      [
 //         'name' => 'Super Admin',
 //         'role' => 'superadmin'
 //      ],
	// 	[
	// 		'name' => 'Admin',
	// 		'role' => 'admin'
	// 	],
	// 	[
	// 		'name' => 'Staff',
	// 		'role' => 'staff'
	// 	],
	// ];

   public function index($restaurantid)
   {
		$data = RestaurantStaff::where('restaurant_id', $restaurantid)->where('is_active', 1)
			->with('users')
			->orderByRaw('CASE WHEN role = "superadmin" THEN 1 WHEN role = "admin" THEN 2 ELSE 3 END')
			->get();

      $restaurant = Restaurant::select('id', 'name')->where('id', $restaurantid)->first();

      return view('restaurant.staff.index', compact('restaurantid', 'data', 'restaurant'));
   }

   // Update Staff Status
   public function status(Request $request, $restaurantid)
   {
      $auth = RestaurantStaff::restaurantRole($restaurantid);
      if($auth == 'superadmin' || $auth == 'admin') {
         $data = RestaurantStaff::updateStatus($restaurantid, $request);
      }   	

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Staff Status Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	} else {
   		session()->flash('message', 'Fail to Update Staff Status');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Retrieve In-Active Staff
   public function archived($restaurantid)
   {
		$data    = RestaurantStaff::where('restaurant_id', $restaurantid)->where('is_active', 0)->with('users')->get();
      $restaurant = Restaurant::select('id', 'name')->where('id', $restaurantid)->first();
		$role    = RestaurantStaff::restaurantRole($restaurantid);
   	return view('restaurant.staff.archived', compact('restaurantid', 'data', 'restaurant', 'role'));
   }

   // Edit Profile
   public function profile()
   {
   	$data = User::where('id', auth()->id())->with('restaurantstaff')->first();
   	return view('restaurant.staff.profile', compact('data'));
   }
}
