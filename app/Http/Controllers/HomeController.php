<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Restaurant;
use App\Http\Models\RestaurantStaff;
use Auth;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->dashboard();
        // return view('home');
    }

    public function dashboard()
    {
        $role = Auth::user()->role;
        
        if($role == 'superadmin' || $role == 'admin') {
            return view('dashboard');

        } else {
            $menu = RestaurantStaff::select('restaurant_id', 'role')->where('user_id', auth()->id())->with('restaurants')->get();
            return view('restaurant.dashboard', compact('menu'));
        }
        
    }

    public function error()
    {
        return view('error');
    }
}
