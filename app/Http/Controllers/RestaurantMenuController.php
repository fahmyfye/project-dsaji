<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantMenu;
use App\Http\Models\RestaurantCategory;
use App\Http\Models\RestaurantIngredient;
use Str;
use Storage;

class RestaurantMenuController extends Controller
{
   public function index($restaurantid)
   {
   	$menu = RestaurantMenu::where('restaurant_id', $restaurantid)->orderBy('category')->orderBy('is_active', 'desc')->get();
   	$data = $menu->map(function ($value, $key) {
   		$value['category'] = RestaurantCategory::where('id', $value['category'])->value('name');
   	   return $value;
   	});

      $role = RestaurantStaff::restaurantRole($restaurantid);

   	return view('restaurant.menu.index', compact('data', 'role', 'restaurantid'));
   }

   public function create($restaurantid)
   {
      $ingredient = RestaurantIngredient::select('id', 'name')->where('is_active', 1)->get();
      $category   = RestaurantCategory::select('id', 'name')->where('is_active', 1)->get();
   	return view('restaurant.menu.create', compact('ingredient', 'category', 'restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
      // Validate
      $request->validate([
         'name'            => 'required|string|max:255',
         'price'           => 'required|numeric',
         'description'     => 'required|string',
         'category'        => 'required',
         'image'           => 'image|max:2048|mimes:png,jpeg',
         'promotion_price' => 'numeric',
      ]);

      // Filter null values in Ingredient Group
      $ingredients = null;
      if(isset($request->ingredient_group)) {
         foreach($request->ingredient_group as $key => $value) {
            if($value['id'] != null) {
               $filtered[$key]['id'] = $value['id'];
               $filtered[$key]['quantity'] = $value['quantity'];
            }            
         }

         $ingredients = json_encode($filtered);
      }

      if($request->file('image')) {
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_menu.'.$ext;

         // Set Path 
         $link = '/images/menu/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

         if($saveimg) {       
            $data = RestaurantMenu::create([
               'restaurant_id'   => $restaurantid,
               'name'            => $request->name,
               'price'           => $request->price,
               'description'     => $request->description,
               'ingredients'     => $ingredients,
               'category'        => $request->category,
               'image'           => $path,
               'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
               'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
               'is_active'       => isset($request->is_active) ? 1 : 0,
            ]);

         } else {
            session()->flash('message', 'Fail to Save Menu Image');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         $data = RestaurantMenu::create([
            'restaurant_id'   => $restaurantid,
            'name'            => $request->name,
            'price'           => $request->price,
            'description'     => $request->description,
            'ingredients'     => $ingredients,
            'category'        => $request->category,
            'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
            'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
            'is_active'       => isset($request->is_active) ? 1 : 0,
         ]);
      }      

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Added');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.menu', $restaurantid);
      }
      else {
         session()->flash('message', 'Fail to Add Menu');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
      $data              = RestaurantMenu::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->ingredients = json_decode($data->ingredients);
      foreach($data->ingredients as $value) {
         $value->name = RestaurantIngredient::where('id', $value->id)->value('name');
      }
      $ingredient = RestaurantIngredient::select('id', 'name')->where('is_active', 1)->get();
      $category   = RestaurantCategory::select('id', 'name')->where('is_active', 1)->get();

      return view('restaurant.menu.edit', compact('data', 'ingredient', 'category', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
      // Validate
      $request->validate([
         'name'            => 'required|string|max:255',
         'price'           => 'required|numeric',
         'description'     => 'required|string',
         'category'        => 'required',
         'image'           => 'image|max:2048|mimes:png,jpeg',
         'promotion_price' => 'numeric',
      ]);

      // Check for null values in Ingredient Group
      $ingredients = null;
      if(isset($request->ingredient_group)) {
         foreach($request->ingredient_group as $key => $value) {
            if($value['id'] != null) {
               $filtered[$key]['id'] = $value['id'];
               $filtered[$key]['quantity'] = $value['quantity'];
            }            
         }

         $ingredients = json_encode($filtered);
      }

      if($request->file('image')) {
         // Delete Old Image
         $old = RestaurantMenu::where('id', $request->id)->value('image');
         $oldimg = str_replace('/storage', '', $old);
         $delimg = Storage::disk('local')->delete($oldimg, 'public');

         // Get File
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_menu.'.$ext;

         // Set Path 
         $link = '/images/menu/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         // Save Image to Storage
         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

         if($saveimg) {       
            $data = RestaurantMenu::where('id', $request->id)->update([
               'restaurant_id'   => $restaurantid,
               'name'            => $request->name,
               'price'           => $request->price,
               'description'     => $request->description,
               'ingredients'     => $ingredients,
               'category'        => $request->category,
               'image'           => $path,
               'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
               'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
               'is_active'       => isset($request->is_active) ? 1 : 0,
            ]);

         } else {
            session()->flash('message', 'Invalid Image');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         $data = RestaurantMenu::where('id', $request->id)->update([
            'restaurant_id'   => $restaurantid,
            'name'            => $request->name,
            'price'           => $request->price,
            'description'     => $request->description,
            'ingredients'     => $ingredients,
            'category'        => $request->category,
            'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
            'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
            'is_active'       => isset($request->is_active) ? 1 : 0,
         ]);
      }      

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.menu', $restaurantid);
      }
      else {
         session()->flash('message', 'Fail to Update Menu');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function info($restaurantid, $id)
   {
      $data              = RestaurantMenu::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->ingredients = json_decode($data->ingredients);
      foreach($data->ingredients as $value) {
         $value->name = RestaurantIngredient::where('id', $value->id)->value('name');
      }
      $data->category    = RestaurantCategory::where('id', $data->category)->value('name');
      return $data;
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantMenu::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Deleted');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();
      }
      else {
         session()->flash('message', 'Fail to Delete Menu');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }
}
