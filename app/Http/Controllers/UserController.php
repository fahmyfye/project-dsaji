<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Role;
use Auth;
use Hash;

class UserController extends Controller
{
	// Return Users List
   public function index()
   {
   	$data = User::where('is_active', 1)->orderByRaw('CASE WHEN role = "superadmin" THEN 1 WHEN role = "admin" THEN 2 ELSE 3 END')->get();
   	return view('user.index', compact('data'));
   }

   // Return Create User Page
   public function create()
   {
   	$roles = Role::all();
   	return view('user.create', compact('roles'));
   }

   // Save User
   public function store(Request $request)
   {
   	// Validate Request
   	$validate = $request->validate([
			'name'     => 'required|string|max:255',
			'email'    => 'required|email|max:255|unique:users',
			'password' => 'required|min:6',
			'phone'    => 'required|string|max:255',
			'role'     => 'required|string',
			'dob'      => 'required|string',
   	]);

   	// Create User Data
   	$data = User::create([
			'name'     => ucwords($request->name),
			'email'    => strtolower($request->email),
			'phone'    => $request->phone,
			'password' => Hash::make($request->password),
			'role'     => $request->role,
			'dob'      => $request->dob,
		]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'User Registration Completed');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('users');
   	}
   	else {
   		session()->flash('message', 'Fail to Register User');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Return Edit User Page
   public function edit($id)
   {
   	$data = User::where('id', $id)->first();
   	$roles = Role::all();
   	return view('user.edit', compact('data', 'roles'));
   }

   // Update User
   public function update(Request $request)
   {
   	// Validate Request
   	$validate = $request->validate([
			'name'   => 'required|string|max:255',
			'email'  => 'required|string|email|max:255|unique:users,email,'.$request->id,
			'phone'  => 'required|string|max:255',
			'role'   => 'required|string',
			'dob'    => 'required|string',
   	]);

   	$data = User::where('id', $request->id)->update([
			'name'   => ucwords($request->name),
			'email'  => strtolower($request->email),
			'phone'  => $request->phone,
			'role'   => $request->role,
			'dob'    => $request->dob,
   	]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'User Profile Updated');
   		session()->flash('alert-class', 'alert-success');
   		if(isset($request->profile)) {
   			return redirect()->route('users.profile');

   		} else {
   			return redirect()->route('users.edit', $request->id);
   		}
   		
   	} else {
   		session()->flash('message', 'Fail to Update User');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Delete User
   public function delete(Request $request)
   {
   	$data = User::where('id', $request->id)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'User Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete User');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Update User Status
   public function status(Request $request)
   {
   	$data = User::where('id', $request->id)->update(['is_active' => $request->is_active]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'User Status Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Update User Status');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Retrieve In-Active Users
   public function archived()
   {
   	$data = User::where('is_active', 0)->get();
   	return view('user.archived', compact('data'));
   }

   // Get User Change Password Page
   public function getpassword()
   {
      return view('user.password');
   }

   // Update Password
   public function password(Request $request)
   {
      // Validate Request
      $validate = $request->validate([
         'current'  => 'required|min:6',
         'password' => 'required|confirmed|min:6',
      ]);

      if (Hash::check($request->current, auth()->user()->password)) {
         $data = User::where('id', Auth::id())->update([
            'password' => Hash::make($request->password)
         ]);

         // Return Response
         if($data) {
            session()->flash('message', 'Password Updated');
            session()->flash('alert-class', 'alert-success');
            return redirect()->back();

         } else {
            session()->flash('message', 'Fail to Update Password');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         session()->flash('message', 'Wrong Password');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   	
   }

   // Reset User Password by Admin
   public function reset(Request $request)
   {
   	$data = User::where('id', $request->id)->update(['password' => Hash::make('123456')]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Password Reset');
   		session()->flash('alert-class', 'alert-success');
         return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Reset Password');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Edit Profile
   public function profile()
   {
   	$data = User::where('id', Auth::id())->with('roles')->first();
   	return view('user.profile', compact('data'));
   }
}
