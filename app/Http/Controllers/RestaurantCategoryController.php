<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantCategory;
use Str;
use Storage;

class RestaurantCategoryController extends Controller
{
   public function index($restaurantid)
   {
      $data = RestaurantCategory::where('restaurant_id', $restaurantid)->orderBy('is_active', 'desc')->orderBy('sorting')->get();
   	return view('restaurant.category.index', compact('restaurantid', 'data'));
   }

   public function create($restaurantid)
   {
      return view('restaurant.category.create', compact('restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
         'name'    => 'required|string|max:255',
         'image'   => 'image|max:2048|mimes:png,jpeg',
         'sorting' => 'numeric',
      ]);

      if($request->file('image')) {
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_category.'.$ext;

         // Set Path 
         $link = '/images/category/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

         if($saveimg) {       
            $data = RestaurantCategory::create([
               'restaurant_id' => $restaurantid,
               'name'          => ucwords($request->name),
               'image'         => $path,
               'sorting'       => $request->sorting,
            ]);
         } else {
            session()->flash('message', 'Fail to Save Category Image');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         $data = RestaurantCategory::create([
            'restaurant_id' => $restaurantid,
            'name'          => ucwords($request->name),
            'image'         => null,
            'sorting'       => $request->sorting,
         ]);
      }

   	// Return Response
      if($data) {
         session()->flash('message', 'Category Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.category', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Save Category');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
   	$data = RestaurantCategory::where('id', $id)->where('restaurant_id', $restaurantid)->first();
   	return view('restaurant.category.edit', compact('data', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
         'name'         => 'required|string|max:255',
         'image'        => 'image|max:2048|mimes:png,jpeg',
         'sorting'      => 'numeric',
      ]);

      if($request->file('image')) {
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_cover.'.$ext;

         // Set Path
         $link = '/images/category/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

      } else {
      	$path = RestaurantCategory::where('id', $request->id)->where('restaurant_id', $restaurantid)->value('image');
      }

   	$data = RestaurantCategory::where('id', $request->id)->update([
			'name'      => $request->name,
			'image'     => $path,
			'sorting'   => $request->sorting,
			'is_active' => isset($request->is_active) ? 1 : 0,
   	]);

   	// Return Response
      if($data) {
         session()->flash('message', 'Category Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.category', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Update Category');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function delete(Request $request, $restaurantid)
   {
      $restaurantid = RestaurantStaff::restaurantId();
      $data         = RestaurantCategory::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Category Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete Category');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }
}
