<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantPettyCash;
use Carbon\Carbon;

class RestaurantPettyCashController extends Controller
{
   public function index($restaurantid)
   {
      $data             = RestaurantPettyCash::where('restaurant_id', $restaurantid)->get();
      if(count($data) > 0) {
         foreach($data as $value) {
            $value->cash       = json_decode($value->cash);
            $value->additional = json_decode($value->additional);
            $value->closing    = json_decode($value->closing);            
         }
      }

   	return view('restaurant.pettycash.index', compact('restaurantid', 'data'));
   }

   public function create($restaurantid)
   {
      $date = Carbon::now()->timezone('Asia/Kuala_Lumpur')->format('d/m/Y');
      return view('restaurant.pettycash.create', compact('restaurantid', 'date'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'date' => 'required',
      ]);

      // Check if date already exist
      $check = RestaurantPettyCash::where('date', $request->date)->exists();

      // If date exist
      if($check == true) {
         session()->flash('message', 'Fail to Add Petty Cash. Date Existed');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back()->withInput();

      }

      $notes = [
         'myr100' => ($request->myr100 != null) ? $request->myr100 : 0,
         'myr50'  => ($request->myr50 != null) ? $request->myr50 : 0,
         'myr20'  => ($request->myr20 != null) ? $request->myr20 : 0,
         'myr10'  => ($request->myr10 != null) ? $request->myr10 : 0,
         'myr5'   => ($request->myr5 != null) ? $request->myr5 : 0,
         'myr1'   => ($request->myr1 != null) ? $request->myr1 : 0,
         'myr050' => ($request->myr050 != null) ? $request->myr050 : 0,
         'myr020' => ($request->myr020 != null) ? $request->myr020 : 0,
         'myr010' => ($request->myr010 != null) ? $request->myr010 : 0,
         'myr005' => ($request->myr005 != null) ? $request->myr005 : 0,
      ];

      $cash = json_encode($notes);

      // Add menu into $request collection
      $request->merge(['cash' => $cash]);

   	$data = RestaurantPettyCash::create([
         'restaurant_id' => $restaurantid,
         'date'          => $request->date,
         'cash'          => $request->cash
   	]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Petty Cash Added');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('restaurants.pettycash', $restaurantid);
   	}
   	else {
   		session()->flash('message', 'Fail to Add Petty Cash');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }

   public function edit($restaurantid, $id)
   {
      $data          = RestaurantPettyCash::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->cash    = json_decode($data->cash);
      $data->remarks = $data->edit_remarks;
      return view('restaurant.pettycash.edit', compact('restaurantid', 'data'));
   }

   public function additional($restaurantid, $id)
   {
      $data             = RestaurantPettyCash::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->additional = json_decode($data->additional);
      $data->remarks    = $data->additional_remarks;
   	return view('restaurant.pettycash.additional', compact('restaurantid', 'data'));
   }

   public function closing($restaurantid, $id)
   {
      $data          = RestaurantPettyCash::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->closing = json_decode($data->closing);
      $data->remarks = $data->closing_remarks;
      return view('restaurant.pettycash.closing', compact('restaurantid', 'data'));
   }

   public function update(Request $request, $restaurantid)
   {
      // Validate
      $request->validate([
         'id'   => 'required',
         'type' => 'required',
      ]);

      $notes = [
         'myr100' => ($request->myr100 != null) ? $request->myr100 : 0,
         'myr50'  => ($request->myr50 != null) ? $request->myr50 : 0,
         'myr20'  => ($request->myr20 != null) ? $request->myr20 : 0,
         'myr10'  => ($request->myr10 != null) ? $request->myr10 : 0,
         'myr5'   => ($request->myr5 != null) ? $request->myr5 : 0,
         'myr1'   => ($request->myr1 != null) ? $request->myr1 : 0,
         'myr050' => ($request->myr050 != null) ? $request->myr050 : 0,
         'myr020' => ($request->myr020 != null) ? $request->myr020 : 0,
         'myr010' => ($request->myr010 != null) ? $request->myr010 : 0,
         'myr005' => ($request->myr005 != null) ? $request->myr005 : 0,
      ];

      $cash = json_encode($notes);

      // Add menu into $request collection
      $request->merge([$request->type => $cash]);

      if($request->type == 'cash') {
         $data = RestaurantPettyCash::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
            'cash'         => $request->cash,
            'edit_remarks' => $request->remarks
         ]);

      } elseif($request->type == 'additional') {
         $data = RestaurantPettyCash::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
            'additional'         => $request->additional,
            'additional_remarks' => $request->remarks
         ]);

      } elseif($request->type == 'closing') {
         $data = RestaurantPettyCash::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
            'closing'         => $request->closing,
            'closing_remarks' => $request->remarks
         ]);

      } else {
         session()->flash('message', 'Update method does not exist');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Petty Cash Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('restaurants.pettycash', $restaurantid);

   	} else {
   		session()->flash('message', 'Fail to Update Petty Cash');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantPettyCash::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Petty Cash Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete Petty Cash');
   		session()->flash('alert-class', 'alert-warning');
   	}
   }
}
