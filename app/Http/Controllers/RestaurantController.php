<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Restaurant;

class RestaurantController extends Controller
{
	const ROLES = 
	[
      [
         'name' => 'Restaurant Super Admin',
         'slug' => 'superadmin'
      ],
		[
			'name' => 'Restaurant Admin',
			'slug' => 'admin'
		],
		[
			'name' => 'Restaurant Staff',
			'slug' => 'staff'
		],
	];

   const DAYS = 
   [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
   ];

   public function index()
   {
   	$data = Restaurant::where('is_active', 1)->get();
   	return view('restaurant.profile.index', compact('data'));
   }

   public function create()
   {
   	$days = self::DAYS;
   	return view('restaurant.profile.create', compact('days'));
   }

   public function store(Request $request)
   {
   	// Validate
      $request->validate([
         'name'      => 'required|string|max:255',
         'nickname'  => 'required|string|max:15',
         'email'     => 'required|email',
         'phone'     => 'required',
         'address1'  => 'required|string|max:255',
         'address2'  => 'required|string|max:255',
         'address3'  => 'nullable|string|max:255',
         'postcode'  => 'required|numeric|digits:5',
         'city'      => 'required|string|max:255',
         'state'     => 'required|string|max:255',
         'country'   => 'required|string|max:255',
         'ops_start' => 'required|string|max:255',
         'ops_end'   => 'required|string|max:255',
         'ops_day'   => 'required',
      ]);

   	$data = Restaurant::create([
			'name'        => ucwords($request->name),
         'nickname'    => ucwords($request->nickname),
			'email'       => strtolower($request->email),
			'phone'       => $request->phone,
			'address1'    => $request->address1,
			'address2'    => $request->address2,
			'address3'    => $request->address3,
			'postcode'    => $request->postcode,
			'city'        => $request->city,
			'state'       => $request->state,
			'country'     => $request->country,
			'ops_start'   => $request->ops_start,
			'ops_end'     => $request->ops_end,
			'ops_day'     => json_encode($request->ops_day),
			'ops_holiday' => isset($request->ops_holiday) ? 1 : 0,
		]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Registration Completed');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('restaurants.index');
   	}
   	else {
   		session()->flash('message', 'Fail to Register Restaurant');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   public function edit($id)
   {
   	$data = Restaurant::where('id', $id)->first();
   	$days = self::DAYS;

   	return view('restaurant.profile.edit', compact('data', 'days'));
   }

   public function update(Request $request)
   {
   	// Validate
      $request->validate([
			'name'      => 'required|string|max:255',
         'nickname'  => 'required|string|max:15',
			'email'     => 'required|email',
			'phone'     => 'required',
			'address1'  => 'required|string|max:255',
			'address2'  => 'required|string|max:255',
			'address3'  => 'nullable|string|max:255',
			'postcode'  => 'required|numeric|digits:5',
			'city'      => 'required|string|max:255',
			'state'     => 'required|string|max:255',
			'country'   => 'required|string|max:255',
			'ops_start' => 'required|string|max:255',
			'ops_end'   => 'required|string|max:255',
			'ops_day'   => 'required',
      ]);

   	$data = Restaurant::where('id', $request->id)->update([
			'name'        => ucwords($request->name),
         'nickname'    => ucwords($request->nickname),
			'email'       => strtolower($request->email),
			'phone'       => $request->phone,
			'address1'    => $request->address1,
			'address2'    => $request->address2,
			'address3'    => ($request->address3) ? $request->address3 : null,
			'postcode'    => $request->postcode,
			'city'        => $request->city,
			'state'       => $request->state,
			'country'     => $request->country,
			'ops_start'   => $request->ops_start,
			'ops_end'     => $request->ops_end,
			'ops_day'     => json_encode($request->ops_day),
			'ops_holiday' => isset($request->ops_holiday) ? 1 : 0,
			'is_active'   => isset($request->is_active) ? 1 : 0,
   	]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->route('restaurants.index');
   	}
   	else {
   		session()->flash('message', 'Fail to Update Restaurant');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }

   public function delete(Request $request)
   {
   	$data = Restaurant::where('id', $request->id)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete Restaurant');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   // Update User Status
   public function status(Request $request)
   {
   	$data = Restaurant::where('id', $request->id)->update(['is_active' => $request->is_active]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Status Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Update Restaurant Status');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }

   public function archived()
   {
   	$data = Restaurant::where('is_active', 0)->get();
   	return view('restaurant.profile.archived', compact('data'));
   }
}
