<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantTable;
use QrCode;

class RestaurantTableController extends Controller
{
   public function index($restaurantid)
   {
		$data = RestaurantTable::where('restaurant_id', $restaurantid)->orderBy('is_active', 'desc')->get();
      if(count($data) > 0) {
         foreach ($data as $value) {
            $value->url = url()->current().'/'.$value->id;
         }
      }      
   	return view('restaurant.table.index', compact('data', 'restaurantid'));
   }

   public function create($restaurantid)
   {
      return view('restaurant.table.create', compact('restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'name' => 'required|string|max:255',
      ]);

		$data = RestaurantTable::create([
			'restaurant_id' => $restaurantid,
			'name'          => ucwords($request->name),
      ]);	

   	// Return Response
      if($data) {
         session()->flash('message', 'Table Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.table', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Save Table');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
   	$data = RestaurantTable::where('id', $id)->where('restaurant_id', $restaurantid)->first();
   	return view('restaurant.table.edit', compact('data', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
         'name'     => 'required|string|max:255',
      ]);

   	$data = RestaurantTable::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
			'name'          => ucwords($request->name),
			'is_active'     => isset($request->is_active) ? 1 : 0,
   	]);

   	// Return Response
      if($data) {
         session()->flash('message', 'Table Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.table', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Update Table');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantTable::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Table Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete Table');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }
}
