<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantIngredient;

class RestaurantIngredientController extends Controller
{
   public function index($restaurantid)
   {
		$data = RestaurantIngredient::where('restaurant_id', $restaurantid)->orderBy('is_active', 'desc')->orderBy('name')->get();
   	return view('restaurant.ingredient.index', compact('restaurantid', 'data'));
   }

   public function create($restaurantid)
   {
      return view('restaurant.ingredient.create', compact('restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'name'     => 'required|string|max:255',
			'quantity' => 'numeric',
      ]);

		$data = RestaurantIngredient::create([
			'restaurant_id' => $restaurantid,
			'name'          => ucwords($request->name),
			'quantity'      => $request->quantity,
      ]);	

   	// Return Response
      if($data) {
         session()->flash('message', 'Ingredient Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.ingredient', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Save Ingredient');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
   	$data = RestaurantIngredient::where('id', $id)->where('restaurant_id', $restaurantid)->first();
   	return view('restaurant.ingredient.edit', compact('data', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
         'name'     => 'required|string|max:255',
			'quantity' => 'numeric',
      ]);

   	$data = RestaurantIngredient::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
			'name'          => ucwords($request->name),
			'quantity'      => $request->quantity,
			'is_active'     => isset($request->is_active) ? 1 : 0,
   	]);

   	// Return Response
      if($data) {
         session()->flash('message', 'Ingredient Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.ingredient', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Update Ingredient');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantIngredient::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Ingredient Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	}
   	else {
   		session()->flash('message', 'Fail to Delete Ingredient');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }
}
