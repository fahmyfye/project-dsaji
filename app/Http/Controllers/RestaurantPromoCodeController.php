<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantPromoCode;
use App\Http\Models\RestaurantPromoCodeRedeem;
use Carbon\Carbon;

class RestaurantPromoCodeController extends Controller
{
   public function index($restaurantid)
   {
      $data = RestaurantPromoCode::where('restaurant_id', $restaurantid)->orderBy('is_active', 'desc')->get();
      if(count($data) > 0) {
         foreach($data as $value) {
            $value->redeemed = RestaurantPromoCodeRedeem::redeemCount($restaurantid, $value->code);
         }
      }
      $userrole = RestaurantStaff::where('restaurant_id', $restaurantid)->where('user_id', auth()->id())->value('role');

   	return view('restaurant.promocode.index', compact('data', 'restaurantid', 'userrole'));
   }

   public function create($restaurantid)
   {
   	return view('restaurant.promocode.create', compact('restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'name'       => 'required|string|max:255',
			'code'       => 'required|alpha_dash|max:255|unique:restaurant_promo_codes',
			'type'       => 'required|string|max:255',
			'value'      => 'required|numeric',
			'allocation' => 'numeric',
      ]);

		$data = RestaurantPromoCode::create([
			'restaurant_id'  => $restaurantid,
			'name'           => $request->name,
			'code'           => $request->code,
			'type'           => $request->type,
			'value'          => $request->value,
			'allocation'     => $request->allocation,
			'is_limited'     => isset($request->is_limited) ? 1 : 0,
			'multiple_usage' => isset($request->multiple_usage) ? 1 : 0,
			'is_active'      => isset($request->is_active) ? 1 : 0,
			'created_by'     => auth()->id(),
      ]);

      if($data) {
         session()->flash('message', 'Promo Code Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.promocode', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Save Promo Code');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
   	$data = RestaurantPromoCode::where('id', $id)->where('restaurant_id', $restaurantid)->first();
   	return view('restaurant.promocode.edit', compact('data', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'name'       => 'required|string|max:255',
			'code'       => 'required|alpha_dash|max:255|unique:restaurant_promo_codes,code,'.$request->id,
			'type'       => 'required|string|max:255',
			'value'      => 'required|numeric',
			'allocation' => 'required|numeric',
      ]);

		$data    = RestaurantPromoCode::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
			'name'           => $request->name,
			'code'           => $request->code,
			'type'           => $request->type,
			'value'          => $request->value,
			'allocation'     => $request->allocation,
			'is_limited'     => isset($request->is_limited) ? 1 : 0,
			'multiple_usage' => isset($request->multiple_usage) ? 1 : 0,
			'is_active'      => isset($request->is_active) ? 1 : 0,
			'updated_by'     => auth()->id(),
      ]);

		if($data) {
         session()->flash('message', 'Promo Code Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.promocode', $restaurantid);

      } else {
         session()->flash('message', 'Fail to Update Promo Code');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantPromoCode::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

      if($data) {
         session()->flash('message', 'Promo Code Deleted');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();

      } else {
         session()->flash('message', 'Fail to Delete Promo Code');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }
}
