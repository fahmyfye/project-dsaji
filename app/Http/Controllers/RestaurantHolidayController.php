<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantHoliday;
use Carbon\Carbon;

class RestaurantHolidayController extends Controller
{
   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'ops_date'  => 'required',
      ]);

		$data = RestaurantHoliday::create([
			'restaurant_id' => $restaurantid,
			'ops_date'      => $request->ops_date,
			'ops_start'     => ($request->ops_start == null) ? null : Carbon::parse($request->ops_start)->format('H:i:s'),
			'ops_end'       => ($request->ops_end == null) ? null : Carbon::parse($request->ops_end)->format('H:i:s'),
      ]);	

   	// Return Response
      if($data) {
         session()->flash('message', 'Restaurant Holiday Operation Details Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();

      } else {
         session()->flash('message', 'Fail to Save Restaurant Holiday Operation Details');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function status(Request $request, $restaurantid)
   {
   	$data = RestaurantHoliday::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
			'is_active' => $request->is_active,
   	]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Holiday Operation Details Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	} else {
   		session()->flash('message', 'Fail to Update Restaurant Holiday Operation Details');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantHoliday::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Holiday Operation Details Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();

   	} else {
   		session()->flash('message', 'Fail to Delete Holiday Operation Details');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }
}
