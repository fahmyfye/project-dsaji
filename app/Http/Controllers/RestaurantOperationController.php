<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantOperation;
use App\Http\Models\RestaurantHoliday;
use Carbon\Carbon;

class RestaurantOperationController extends Controller
{
	const DAYS = 
	[
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
		'Sunday'
	];

   public function index($restaurantid) 
   {
		$data = RestaurantOperation::where('restaurant_id', $restaurantid)->get();
		$days = self::DAYS;

		if(count($data) > 0) {
			foreach($data as $value) {
				// Get Operation Days
				$decode           = json_decode($value->ops_day);
				$value->ops_day   = implode(', ', $decode);
				$value->ops_start = Carbon::parse($value->ops_start)->format('g:i A');
				$value->ops_end   = Carbon::parse($value->ops_end)->format('g:i A');

				// Remove Set Operation Days from Preset 'DAYS'
				$days = array_diff($days, $decode);
			}
		}

      $holiday = RestaurantHoliday::where('restaurant_id', $restaurantid)->get();

   	return view('restaurant.operation.index', compact('restaurantid', 'data', 'days', 'holiday'));
   }

   public function store(Request $request, $restaurantid)
   {
   	// Validate
      $request->validate([
			'ops_day'   => 'required',
			'ops_start' => 'required|string|max:255',
			'ops_end'   => 'required|string|max:255',
      ]);

		$data = RestaurantOperation::create([
			'restaurant_id' => $restaurantid,
			'ops_day'       => json_encode($request->ops_day),
			'ops_start'     => Carbon::parse($request->ops_start)->format('H:i:s'),
			'ops_end'       => Carbon::parse($request->ops_end)->format('H:i:s'),
      ]);	

   	// Return Response
      if($data) {
         session()->flash('message', 'Restaurant Operation Details Saved');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();

      } else {
         session()->flash('message', 'Fail to Save Restaurant Operation Details');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function status(Request $request, $restaurantid)
   {
   	$data = RestaurantOperation::where('id', $request->id)->where('restaurant_id', $restaurantid)->update([
			'is_active' => $request->is_active,
   	]);

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Restaurant Operation Details Updated');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();
   	} else {
   		session()->flash('message', 'Fail to Update Restaurant Operation Details');
   		session()->flash('alert-class', 'alert-warning');
   		return redirect()->back();
   	}
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantOperation::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

   	// Return Response
   	if($data) {
   		session()->flash('message', 'Operation Details Deleted');
   		session()->flash('alert-class', 'alert-success');
   		return redirect()->back();

   	} else {
   		session()->flash('message', 'Fail to Delete Operation Details');
   		session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
   	}
   }
}
