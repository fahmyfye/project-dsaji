<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\RestaurantStaff;
use App\Http\Models\RestaurantBundle;
use App\Http\Models\RestaurantCategory;
use App\Http\Models\RestaurantMenu;
use Str;
use Storage;

class RestaurantBundleController extends Controller
{
   public function index($restaurantid)
   {
   	$bundle = RestaurantBundle::where('restaurant_id', $restaurantid)->orderBy('is_active', 'desc')->orderBy('sorting')->get();
      $data = $bundle->map(function ($value, $key) {
         $value['category'] = RestaurantCategory::where('id', $value['category'])->value('name');
         $value['menus'] = json_decode($value['menus']);
         foreach($value['menus'] as $item) {
            $item->name = RestaurantMenu::where('id', $item->id)->value('name');
         }
         return $value;
      });

      $role = RestaurantStaff::restaurantRole($restaurantid);
   	
   	return view('restaurant.bundle.index', compact('data', 'role', 'restaurantid'));
   }

   public function create($restaurantid)
   {
      $menu     = RestaurantMenu::select('id', 'name')->where('restaurant_id', $restaurantid)->where('is_active', 1)->get();
      $category = RestaurantCategory::select('id', 'name')->where('is_active', 1)->get();
      return view('restaurant.bundle.create', compact('menu', 'category', 'restaurantid'));
   }

   public function store(Request $request, $restaurantid)
   {
      // Filter null values in Ingredient Group
      $menus = null;
      if(isset($request->menu_group)) {
         foreach($request->menu_group as $key => $value) {
            if($value['id'] != null) {
               $filtered[$key]['id'] = $value['id'];
               $filtered[$key]['quantity'] = $value['quantity'];
            }            
         }

         $menus = json_encode($filtered);
      }

      // Add menu into $request collection
      $request->merge(['menus' => $menus]);

      // Validate
      $request->validate([
         'name'            => 'required|string|max:255',
         'price'           => 'required|numeric',
         'description'     => 'required|string',
         'menus'           => 'required',
         'category'        => 'required',
         'image'           => 'image|max:2048|mimes:png,jpeg',
         'promotion_price' => 'numeric',
      ]);

      // If Image Uploaded
      if($request->file('image')) {
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_menu.'.$ext;

         // Set Path 
         $link = '/images/bundle/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

         if($saveimg) {       
            $data = RestaurantBundle::create([
               'restaurant_id'   => $restaurantid,
               'name'            => $request->name,
               'price'           => $request->price,
               'description'     => $request->description,
               'menus'           => $request->menus,
               'category'        => $request->category,
               'image'           => $path,
               'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
               'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
               'is_active'       => isset($request->is_active) ? 1 : 0,
            ]);

         } else {
            session()->flash('message', 'Fail to Save Bundle Image');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         $data = RestaurantBundle::create([
            'restaurant_id'   => $restaurantid,
            'name'            => $request->name,
            'price'           => $request->price,
            'description'     => $request->description,
            'menus'           => $request->menus,
            'category'        => $request->category,
            'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
            'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
            'is_active'       => isset($request->is_active) ? 1 : 0,
         ]);
      }      

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Bundle Added');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.bundle', $restaurantid);
      }
      else {
         session()->flash('message', 'Fail to Add Menu Bundle');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function edit($restaurantid, $id)
   {
      $data        = RestaurantBundle::where('id', $id)->where('restaurant_id', $restaurantid)->first();
      $data->menus = json_decode($data->menus);
      foreach($data->menus as $value) {
         $value->name = RestaurantMenu::where('id', $value->id)->value('name');
      }
      $menu     = RestaurantMenu::select('id', 'name')->where('is_active', 1)->get();
      $category = RestaurantCategory::select('id', 'name')->where('is_active', 1)->get();

      return view('restaurant.bundle.edit', compact('data', 'menu', 'category', 'restaurantid'));
   }

   public function update(Request $request, $restaurantid)
   {
      // Filter null values in Ingredient Group
      $menus = null;
      if(isset($request->menu_group)) {
         foreach($request->menu_group as $key => $value) {
            if($value['id'] != null) {
               $filtered[$key]['id'] = $value['id'];
               $filtered[$key]['quantity'] = $value['quantity'];
            }            
         }

         $menus = json_encode($filtered);
      }

      // Add menu into $request collection
      $request->merge(['menus' => $menus]);

      // Validate
      $request->validate([
         'name'            => 'required|string|max:255',
         'price'           => 'required|numeric',
         'description'     => 'required|string',
         'menus'           => 'required',
         'category'        => 'required',
         'image'           => 'image|max:2048|mimes:png,jpeg',
         'promotion_price' => 'numeric',
      ]);

      // If Image Uploaded
      if($request->file('image')) {
         // Delete Old Image
         $old = RestaurantBundle::where('id', $request->id)->value('image');
         $oldimg = str_replace('/storage', '', $old);
         $delimg = Storage::disk('local')->delete($oldimg, 'public');

         // Get File
         $files = $request->file('image');

         // Get Random String
         $str = Str::random(5);

         // Get File Extension
         $ext = $request->file('image')->getClientOriginalExtension();

         // Set Name
         $name = $str.'_menu.'.$ext;

         // Set Path 
         $link = '/images/bundle/'.$name;
         $path = '/storage'.$link; // (for local storage add 'storage/')

         // Save Image to Storage
         $saveimg = Storage::disk('local')->put($link, file_get_contents($files), 'public');

         if($saveimg) {       
            $data = RestaurantBundle::where('id', $request->id)->update([
               'restaurant_id'   => $restaurantid,
               'name'            => $request->name,
               'price'           => $request->price,
               'description'     => $request->description,
               'menus'           => $request->menus,
               'category'        => $request->category,
               'image'           => $path,
               'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
               'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
               'is_active'       => isset($request->is_active) ? 1 : 0,
            ]);

         } else {
            session()->flash('message', 'Fail to Save Bundle Image');
            session()->flash('alert-class', 'alert-warning');
            return redirect()->back();
         }

      } else {
         $data = RestaurantBundle::where('id', $request->id)->update([
            'restaurant_id'   => $restaurantid,
            'name'            => $request->name,
            'price'           => $request->price,
            'description'     => $request->description,
            'menus'           => $request->menus,
            'category'        => $request->category,
            'is_promotion'    => isset($request->is_promotion) ? 1 : 0,
            'promotion_price' => isset($request->is_promotion) ? $request->promotion_price : 0,
            'is_active'       => isset($request->is_active) ? 1 : 0,
         ]);
      }      

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Bundle Updated');
         session()->flash('alert-class', 'alert-success');
         return redirect()->route('restaurants.bundle', $restaurantid);
      }
      else {
         session()->flash('message', 'Fail to Update Menu Bundle');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }

   public function info($restaurantid, $id)
   {
      $data           = RestaurantBundle::where('restaurant_id', $restaurantid)->where('id', $id)->first();
      $data->category = RestaurantCategory::where('id', $data->category)->value('name');
      $data->menus    = json_decode($data->menus);
      foreach($data->menus as $value) {
         $value->name = RestaurantMenu::where('id', $value->id)->value('name');
      }

      return $data;
   }

   public function delete(Request $request, $restaurantid)
   {
      $data = RestaurantBundle::where('id', $request->id)->where('restaurant_id', $restaurantid)->delete();

      // Return Response
      if($data) {
         session()->flash('message', 'Menu Bundle Deleted');
         session()->flash('alert-class', 'alert-success');
         return redirect()->back();
      }
      else {
         session()->flash('message', 'Fail to Delete Menu Bundle');
         session()->flash('alert-class', 'alert-warning');
         return redirect()->back();
      }
   }
}
