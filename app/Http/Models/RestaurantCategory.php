<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\RestaurantStaff;

class RestaurantCategory extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'image', 'sorting', 'is_active',
   ];

   public function scopeName($query, $id)
   {
   	$query = $this->where('id', $id)->value('name');
   	return $query;
   }
}
