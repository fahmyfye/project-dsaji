<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantTable extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'is_active',
   ];
}
