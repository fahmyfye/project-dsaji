<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantHoliday extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'ops_start', 'ops_end', 'ops_date', 'is_active',
   ];
}
