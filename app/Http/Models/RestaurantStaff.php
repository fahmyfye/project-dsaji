<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\User;

class RestaurantStaff extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'user_id', 'restaurant_id', 'role', 'is_active',
   ];

   public function scopeRestaurantRole($query, $restaurantid) {
      $query = $this->where('restaurant_id', $restaurantid)->where('user_id', auth()->id())->value('role');
      return $query;
   }

   public function scopeCreateStaff($query, $restaurantid, $id, $role)
   {
      $query = $this->create([
         'user_id'       => $id,
         'restaurant_id' => $restaurantid,
         'role'          => $role,
      ]);

      return $query;
   }

   public function scopeGetStaff($query, $id)
   {
      $user_id = $this->where('id', $id)->value('user_id');
      $query   = User::where('id', $user_id)->with('restaurantstaff')->first();
      return $query;
   }

   public function scopeUpdateStatus($query, $restaurantid, $request) {
      $staff = $this->where('restaurant_id', $restaurantid)->where('id', $request->user_id)->update(['is_active' => $request->is_active]);
      if($staff) {
         $user = User::where('id', $request->user_id)->update(['is_active' => $request->is_active]);

         if($user) {
            return $query = true;
         } else {
            return $query = false;
         }
      }
   }

   public function scopeDeleteStaff($query, $request) {
      $staff = $this->where('id', $request->id)->where('restaurant_id', $this->restaurantId())->delete();
      if($staff) {
         $user = User::where('id', $request->user_id)->delete();

         if($user) {
            return $query = true;
         } else {
            return $query = false;
         }
      }
   }

   public function users()
   {
      return $this->hasOne('App\Http\Models\User', 'id', 'user_id')->select(
         'id', 'name', 'email', 'role', 'dob', 'phone', 'is_active'
      );
   }

   public function active()
   {
      return $this->users()->where('is_active', 1);
   }

   public function restaurants()
   {
      return $this->hasOne('App\Http\Models\Restaurant', 'id', 'restaurant_id');
   }
}
