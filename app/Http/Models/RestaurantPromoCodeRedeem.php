<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\RestaurantStaff;

class RestaurantPromoCodeRedeem extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'discount_id', 'code', 'user_id', 'sales_id',
   ];

   public function scopeRedeemCount($query, $restaurantid, $code)
   {
		$query = $this->where('restaurant_id', $restaurantid)->where('code', $code)->count();
   	return $query;
   }
}
