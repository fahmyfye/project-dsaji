<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
   use SoftDeletes;

	protected $fillable = [
      
   ];

   public function users()
   {
      return $this->hasMany('App\Http\Models\User', 'role', 'slug')->where('is_active', 1);
   }
}
