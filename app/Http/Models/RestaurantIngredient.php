<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantIngredient extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'quantity', 'is_active',
   ];

   public function scopeName($query, $id)
   {
   	$query = $this->where('id', $id)->value('name');
   	return $query;
   }
}
