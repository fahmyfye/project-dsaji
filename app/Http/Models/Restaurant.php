<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'name', 'nickname', 'email', 'phone', 'address1', 'address2', 'address3', 'postcode', 'city', 'state', 'country', 'ops_holiday', 'is_active',
   ];

   public function scopeRestaurantName($query, $id)
   {
   	$query = $this->where('id', $id)->value('name');
   	return $query;
   }

   public function staff()
   {
      return $this->hasMany('App\Http\Models\RestaurantStaff', 'restaurant_id', 'id');
   }
}
