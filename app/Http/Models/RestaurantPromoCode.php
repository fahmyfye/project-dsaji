<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantPromoCode extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'code', 'type', 'value', 'allocation', 'is_limited', 'multiple_usage', 'is_active', 'created_by', 'updated_by'
   ];
}
