<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantMenu extends Model
{
   use SoftDeletes;

	protected $fillable = [
      'restaurant_id', 'name', 'price', 'description', 'ingredients', 'category', 'image', 'is_promotion', 'promotion_price', 'sorting', 'is_active',
   ];
}