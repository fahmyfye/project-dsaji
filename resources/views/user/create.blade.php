@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">New User</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Users</li>
                     <li class="breadcrumb-item active"><a href="{{ route('users.create') }}">New User</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                  </div>

                  <form class="form form-horizontal" method="post" action="{{ route('users.store') }}">
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> User Account Details</h4>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="email">Email</label>
                           <div class="col-md-9">
                              <input type="email" id="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="phone">Contact No.</label>
                           <div class="col-md-9">
                              <input type="text" id="phone" class="form-control" placeholder="Contact No." name="phone" value="{{ old('phone') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="password">Password</label>
                           <div class="col-md-9">
                              <input type="text" id="password" class="form-control" placeholder="Password" name="password">
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="role">Access Level</label>
                           <div class="col-md-9">
                              @if(!empty($roles))
                                 <select class="form-control select2-placeholder" id="role" name="role">
                                    @foreach($roles as $value)
                                       <option value="{!! $value->slug !!}">{!! $value->name !!}</option>
                                    @endforeach
                                 </select>
                              @else
                                 <small>Please add roles</small>
                              @endif
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="role">Date of Birth</label>
                           <div class="col-md-9">
                              <div class='input-group date'>
                                  <input type="text" class="form-control" id="dob" name="dob" placeholder="Date of Birth" value="{{ old('dob') }}"  />
                                  <div class="input-group-append">
                                      <span class="input-group-text">
                                          <span class="fa fa-calendar"></span>
                                      </span>
                                  </div>
                              </div>
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <a href="{!! route('users') !!}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                        <button type="submit" class="btn btn-primary">
                           <i class="fa fa-check-square-o"></i> Save
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('.select2-placeholder').select2({
         placeholder: 'Select Access Level',
         allowClear: true
      })

      $('#dob').datetimepicker({
         format: 'YYYY-MM-DD'
      });
   })
</script>
@endsection