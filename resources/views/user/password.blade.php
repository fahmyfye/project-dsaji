@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Change Password</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item active"><a href="{{ route('users.password') }}">Change Password</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                  </div>

                  <form class="form form-horizontal" method="post" action="{{ route('users.password.update') }}">
                     @method('patch')
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"><i class="ft-lock"></i> Change User Password</h4>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="current">Current Password</label>
                           <div class="col-md-9">
                              <input type="password" id="current" class="form-control" placeholder="Current Password" name="current" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="password">New Password</label>
                           <div class="col-md-9">
                              <input type="password" id="password" class="form-control" placeholder="New Password" name="password" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="password_confirmation">Confirm Password</label>
                           <div class="col-md-9">
                              <input type="password" id="password_confirmation" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                           <i class="ft-save"></i> Update
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection