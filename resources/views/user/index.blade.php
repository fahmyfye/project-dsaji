@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Registered Users</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Users</li>
                     <li class="breadcrumb-item active"><a href="{{ route('users') }}">Registered Users</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Registered Users</h4>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-hover table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center" width="1%">#</th>
                                 <th>Name</th>
                                 <th class="text-center" width="15%">Email</th>
                                 <th class="text-center" width="5%">Contact No.</th>
                                 <th width="10%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                                 <tr>
                                    <td class="text-center">{!! ($key+1) !!}</td>
                                    <td>{!! $value->name !!}</td>                           
                                    <td class="text-center">{!! $value->email !!}</td>
                                    <td class="text-center">{!! $value->phone !!}</td>
                                    <td class="text-center">
                                       <div class="btn-group" role="group" aria-label="Options">
                                          <!-- Reset User Password -->
                                          <form id="resetform_{!! $value->id !!}" class="form form-horizontal" method="post" action="{{ route('users.reset') }}">
                                             @method('patch')
                                             @csrf
                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <button id="reset_{!! $value->id !!}" class="btn btn-outline-secondary" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Reset User Password (123456)">
                                                <i class="ft-refresh-cw"></i>
                                             </button>
                                          </form>

                                          <!-- Deactivate User -->
                                          <form id="statform_{!! $value->id !!}" class="form form-horizontal" method="post" action="{{ route('users.status') }}">
                                             @method('patch')
                                             @csrf
                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <input type="hidden" name="is_active" value="0">
                                             <button id="status_{!! $value->id !!}" class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate User">
                                                <i class="fa fa-power-off"></i>
                                             </button>
                                          </form>

                                          <!-- Edit User Details -->
                                          <a href="{{ route('users.edit', $value->id) }}" class="btn btn-outline-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit User Details">
                                             <i class="ft-edit"></i>
                                          </a>

                                          @if(Auth::user()->role == 'superadmin')
                                             <!-- Delete User -->
                                             <form id="form_{!! $value->id !!}" class="form form-horizontal" method="post" action="{{ route('users.delete') }}">
                                                @method('delete')
                                                @csrf
                                                <input type="hidden" name="id" value="{!! $value->id !!}">
                                                <button id="delete_{!! $value->id !!}" class="btn btn-outline-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete User">
                                                   <i class="ft-x"></i>
                                                </button>
                                             </form>
                                          @endif
                                       </div>
                                    </td>
                                 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>

                  @endif

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('.select2-placeholder').select2({
         placeholder: 'Select Role',
         allowClear: true
      })

      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
      })
   })
</script>
<script>
   $('#search').click(function(e) {
      e.preventDefault()
      $('#searchform').submit()
   })

   $('[id^=reset_]').click(function(e) {
      e.preventDefault()
      if(confirm('Reset this user\'s password?')) {
         x = this.id
         y = x.replace('reset_', '')
         $('#resetform_'+y).submit()
      }
   })

   $('[id^=status_]').click(function(e) {
      e.preventDefault()
      if(confirm('Deactivate this user?')) {
         x = this.id
         y = x.replace('status_', '')
         $('#statform_'+y).submit()
      }
   })

   $('[id^=delete_]').click(function(e) {
      e.preventDefault()
      if(confirm('Remove this user?')) {
         x = this.id
         y = x.replace('delete_', '')
         $('#form_'+y).submit()
      }
   })
</script>
@endsection