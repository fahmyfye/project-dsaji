<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-dark navbar-shadow">
   <div class="navbar-wrapper">
      <div class="navbar-header">
         <ul class="nav navbar-nav flex-row">
            <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item"><a class="navbar-brand" href="#"><img class="brand-logo" alt="stack admin logo" src="{{ asset('/storage/images/logo_dsaji.png') }}" style="max-height: 32px">
               <h4 class="brand-text">D'Saji</h4>
            </a></li>
            <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
         </ul>
      </div>

      <div class="navbar-container content">
         <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
               <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a>
               </li>
            </ul>

            <ul class="nav navbar-nav float-right">
               <li class="dropdown dropdown-user nav-item">
                  <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                     {{-- <span class="avatar"><img src="{{ asset('/storage/images/logo_dsaji.png') }}" alt="avatar"><i></i></span> --}}
                     <span class="user-name">{{ auth()->user()->name }}</span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                     
                     @if(auth()->user()->role == 'restaurant')
                        <a class="dropdown-item" href="{{ route('restaurants.staff.profile') }}">
                           <i class="ft-user"></i> View Profile
                        </a>

                     @else
                        <a class="dropdown-item" href="{{ route('users.profile') }}">
                           @if(auth()->user()->role == 'superadmin' || auth()->user()->role == 'admin')
                              <i class="ft-user"></i> Edit Profile
                           @else
                              <i class="ft-user"></i> View Profile
                           @endif
                        </a>
                     @endif

                     <a class="dropdown-item" href="{{ route('users.password') }}">
                        <i class="ft-lock"></i> Change Password
                     </a>
                     <div class="dropdown-divider"></div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                           @csrf
                        </form>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); $('#logout-form').submit();">
                           <i class="ft-power"></i> Logout
                        </a>
                  </div>
               </li>
            </ul>
         </div>
      </div>
   </div>
</nav>