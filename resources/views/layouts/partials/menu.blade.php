@if(auth()->user()->role == 'superadmin' || auth()->user()->role == 'admin')
   <!-- Begin: System Admin Menu -->
   <div id="mymenu" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="main-menu-content">
         <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
               <span>Dashboard</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Dashboard"></i>
            </li>
            
            <li class="nav-item {{ request()->routeIs('dashboard') ? 'active' : '' }}">
               <a href="{!! route('dashboard') !!}">
                  <i class="ft-layout"></i><span class="menu-title" data-i18n="">Dashboard</span>
               </a>
            </li>

            <!-- BEGIN: Users -->
            <li class=" nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Users</span></a>
               <ul class="menu-content">
                  <li class="{{ (request()->routeIs('users') || request()->routeIs('users.edit')) ? 'active' : '' }}">
                     <a href="{!! route('users') !!}">
                        <i class="ft-list"></i><span class="menu-title" data-i18n="">List</span>
                     </a>
                  </li>
               </ul>

               <ul class="menu-content">
                  <li class="{{ request()->routeIs('users.create') ? 'active' : '' }}">
                     <a href="{!! route('users.create') !!}">
                        <i class="ft-user-plus"></i><span class="menu-title" data-i18n="">New</span>
                     </a>
                  </li>
               </ul>

               <ul class="menu-content">
                  <li class="{{ request()->routeIs('users.archived') ? 'active' : '' }}">
                     <a href="{!! route('users.archived') !!}">
                        <i class="ft-package"></i><span class="menu-title" data-i18n="">Archived</span>
                     </a>
                  </li>
               </ul>
            </li>
            <!-- END: Users -->

            <!-- BEGIN: Restaurant -->
            @if((Auth::user()->role != 'restaurantstaff') || (Auth::user()->role != 'user'))
            <li class=" nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Restaurant</span></a>
               <ul class="menu-content">
                  <li class="{{ (request()->routeIs('restaurants.index') || request()->routeIs('restaurants.staff')) ? 'active' : '' }}">
                     <a href="{!! route('restaurants.index') !!}">
                        <i class="ft-list"></i><span class="menu-title" data-i18n="">List</span>
                     </a>
                  </li>
               </ul>

               <ul class="menu-content">
                  <li class="{{ request()->routeIs('restaurants.create') ? 'active' : '' }}">
                     <a href="{!! route('restaurants.create') !!}">
                        <i class="ft-user-plus"></i><span class="menu-title" data-i18n="">New</span>
                     </a>
                  </li>
               </ul>

               <ul class="menu-content">
                  <li class="{{ request()->routeIs('restaurants.archived') ? 'active' : '' }}">
                     <a href="{!! route('restaurants.archived') !!}">
                        <i class="ft-package"></i><span class="menu-title" data-i18n="">Archived</span>
                     </a>
                  </li>
               </ul>
            </li>
            @endif
            <!-- END: Restaurant -->
            
            
         </ul>
      </div>
   </div>
   <!-- End: System Admin Menu -->

@else
   <!-- Begin: Restaurant Admin Menu -->
   <div id="mymenu" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="main-menu-content">
         <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
               <span>Dashboard</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Dashboard"></i>
            </li>
            
            <!-- BEGIN: Dashboard -->
            <li class="nav-item {{ request()->routeIs('dashboard') ? 'active' : '' }}">
               <a href="{!! route('dashboard') !!}">
                  <i class="ft-layout"></i><span class="menu-title" data-i18n="">Dashboard</span>
               </a>
            </li>
            <!-- END: Dashboard -->

            <?php !empty(config('restaurant.menu')) ? ($menu = config('restaurant.menu')) : ''; ?>
            @if(!empty($menu))
               @foreach($menu as $value)
                  <li class=" nav-item">
                     <a href="#"><i class="fa fa-industry"></i><span class="menu-title" data-i18n="">{!! $value->restaurants->nickname !!}</span></a>

                     <!-- BEGIN: Orders -->
                     {{-- <ul class="menu-content">
                        <li class="{{ (request()->routeIs('restaurants.orders')) ? 'active' : '' }}">
                           <a href="{!! route('restaurants.orders') !!}">
                              <i class="ft-layers"></i><span class="menu-title" data-i18n="">Orders</span>
                           </a>
                        </li>
                     </ul> --}}
                     <!-- END: Orders -->

                     <!-- BEGIN: Menu -->
                     <ul class="menu-content">
                        <li class="{{ (request()->is('restaurants/menu/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                           <a href="{!! route('restaurants.menu', $value->restaurant_id) !!}">
                              <i class="ft-clipboard"></i><span class="menu-title" data-i18n="">Menu</span>
                           </a>
                        </li>
                     </ul>
                     <!-- END: Menu -->

                     <!-- BEGIN: Bundles -->
                     <ul class="menu-content">
                        <li class="{{ (request()->is('restaurants/bundle/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                           <a href="{!! route('restaurants.bundle', $value->restaurant_id) !!}">
                              <i class="ft-box"></i><span class="menu-title" data-i18n="">Bundles</span>
                           </a>
                        </li>
                     </ul>
                     <!-- END: Bundles -->

                     <!-- BEGIN: Petty Cash -->
                     <ul class="menu-content">
                        <li class="{{ (request()->is('restaurants/pettycash/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                           <a href="{!! route('restaurants.pettycash', $value->restaurant_id) !!}">
                              <i class="fa fa-money"></i><span class="menu-title" data-i18n="">Petty Cash</span>
                           </a>
                        </li>
                     </ul>
                     <!-- END: Petty Cash -->

                     @if($value->role == 'superadmin' || $value->role == 'admin')

                     <ul class="menu-content">
                        <!-- BEGIN: Staff -->
                        <li class=" nav-item"><a href="#"><i class="ft-users"></i><span class="menu-title" data-i18n="">Staff</span></a>
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/staff/'.$value->restaurant_id) ? 'active' : '') }}">
                                 <a href="{!! route('restaurants.staff', $value->restaurant_id) !!}">
                                    <i class="ft-list"></i><span class="menu-title" data-i18n="">List</span>
                                 </a>
                              </li>
                           </ul>

                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/staff/'.$value->restaurant_id.'/archived') ? 'active' : '') }}">
                                 <a href="{!! route('restaurants.staff.archived', $value->restaurant_id) !!}">
                                    <i class="ft-package"></i><span class="menu-title" data-i18n="">Archived</span>
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- END: Staff -->

                        <!-- BEGIN: Settings -->
                        <li class=" nav-item"><a href="#"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Settings</span></a>
                           <!-- BEGIN: Profile -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/profile/'.$value->restaurant_id)) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.profile', $value->restaurant_id) !!}">
                                    <i class="ft-info"></i><span class="menu-title" data-i18n="">Profile</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Profile -->

                           <!-- BEGIN: Operations -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/operations/'.$value->restaurant_id)) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.operations', $value->restaurant_id) !!}">
                                    <i class="ft-clock"></i><span class="menu-title" data-i18n="">Operations</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Operations -->

                           <!-- BEGIN: Ingredients -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/ingredient/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.ingredient', $value->restaurant_id) !!}">
                                    <i class="ft-user-plus"></i><span class="menu-title" data-i18n="">Ingredients</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Ingredients -->

                           <!-- BEGIN: Category -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/category/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.category', $value->restaurant_id) !!}">
                                    <i class="ft-book"></i><span class="menu-title" data-i18n="">Category</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Category -->

                           <!-- BEGIN: Tables -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/table/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.table', $value->restaurant_id) !!}">
                                    <i class="ft-monitor"></i><span class="menu-title" data-i18n="">Tables</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Tables -->

                           <!-- BEGIN: Promo Code -->
                           <ul class="menu-content">
                              <li class="{{ (request()->is('restaurants/promocode/'.$value->restaurant_id.'*')) ? 'active' : '' }}">
                                 <a href="{!! route('restaurants.promocode', $value->restaurant_id) !!}">
                                    <i class="ft-package"></i><span class="menu-title" data-i18n="">Promo Code</span>
                                 </a>
                              </li>
                           </ul>
                           <!-- END: Promo Code -->
                        </li>
                        <!-- END: Settings -->
                     </ul>
                     @endif
                  </li>
               @endforeach
            @endif
         </ul>
      </div>
   </div>
   <!-- End: Restaurant Admin Menu -->
@endif