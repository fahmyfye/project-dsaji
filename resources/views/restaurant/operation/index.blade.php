@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Restaurant Operations</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.operations', $restaurantid) }}">Operations</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <!-- Add Operation Hours -->
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"><i class="ft-clock"></i> Add Restaurant Operation</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a class="card-collapse" data-action="collapse"><i class="ft-minus"></i></a></li>
                   </ul>
               </div>
            </div>
            
            <div class="card-content collapse show" aria-expanded="true">
               <div class="card-body">
                  <div class="card-text"></div>

                  <form class="form form-horizontal" method="post" action="{{ route('restaurants.operations.store', $restaurantid) }}">
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"></h4>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_day">Operation Day</label>
                           <div class="col-md-9">
                              <select class="form-control" multiple="multiple" id="ops_day" name="ops_day[]">
                                 <option></option>
                                 @foreach($days as $day)
                                    <option value="{!! $day !!}">
                                       <strong>+ {!! $day !!}</strong>
                                    </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        
                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_start">Operation Starts</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control ops_start" placeholder="Operation Starts" name="ops_start"  required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_end">Operation Ends</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control ops_end" placeholder="Operation Ends" name="ops_end" required>
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                           <i class="ft-save"></i> Add Operation Hours
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>

         <!-- Operation Hours Details -->
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"><i class="ft-info"></i> Restaurant Operation Details</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a class="card-collapse" data-action="collapse"><i class="ft-minus"></i></a></li>
                   </ul>
               </div>
            </div>
            
            <div class="card-content collapse show" aria-expanded="true">
               <div class="card-body">
                  <div class="card-text">
                  </div>

                  <form class="form form-horizontal">
                     <div class="form-body">
                        <h4 class="form-section"></h4>

                        <div class="form-group row">
                           <label class="col-md-5 label-control text-center" for="ops_day">Days</label>
                           <label class="col-md-1 label-control text-center" for="ops_start">Starts</label>
                           <label class="col-md-1 label-control text-center" for="ops_end">Ends</label>
                           <label class="col-md-1 label-control text-center" for="is_active">Status</label>
                        </div>

                        @empty($data)
                           <div class="alert alert-warning">Please specify operation day</div>
                        @else
                           @foreach($data as $value)
                              <div class="form-group row">
                                 <div class="col-md-5">
                                    <input type="hidden" value="{{ $value->id }}" required>
                                    <input type="text" class="form-control text-center" value="{{ $value->ops_day }}" readonly>
                                 </div>
                                 <div class="col-md-1">
                                    <input type="text" class="form-control text-center ops_start" value="{{ $value->ops_start }}" readonly>
                                 </div>
                                 <div class="col-md-1">
                                    <input type="text" class="form-control text-center ops_end" value="{{ $value->ops_end }}" readonly>
                                 </div>
                                 <div class="col-md-1 text-center">
                                    @if($value->is_active == 1)
                                       <div class="badge badge-success round">
                                          <span>Active</span>
                                          <i class="ft-check font-medium-2"></i>
                                       </div>
                                    @else
                                       <div class="badge badge-danger round">
                                          <span>In-Active</span>
                                          <i class="ft-x font-medium-2"></i>
                                       </div> 
                                    @endif
                                 </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <div class="btn-group" role="group">
                                          <!-- Button group with icons -->
                                          <form></form>
                                          <form id="updateform_{{ $value->id }}" method="post" action="{{ route('restaurants.operations.status', $restaurantid) }}">
                                             @method('patch')
                                             @csrf

                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <input type="hidden" name="is_active" value="{!! ($value->is_active == 1) ? 0 : 1; !!}">
                                             <button id="update_{{ $value->id }}" type="button" {!! ($value->is_active == 1) ? 'class="btn btn-danger"' : 'class="btn btn-success"'; !!} data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate Operation Details"><i class="fa fa-power-off"></i></button>
                                          </form>

                                          <form id="deleteform_{{ $value->id }}" method="post" action="{{ route('restaurants.operations.delete', $restaurantid) }}">
                                             @method('delete')
                                             @csrf

                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <button id="delete_{{ $value->id }}" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Operation Details"><i class="ft-x"></i></button>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           @endforeach
                        @endempty

                     </div>

                     <div class="form-actions">
                     </div>
                  </form>
               </div>
            </div>
         </section>

         <!-- Add Holiday Operation Hours -->
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"><i class="ft-clock"></i> Add Restaurant Holiday Operation</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a class="card-collapse" data-action="collapse"><i class="ft-minus"></i></a></li>
                   </ul>
               </div>
            </div>

            <div class="card-content collapse" aria-expanded="true">
               <div class="card-body">
                  <div class="card-text"></div>

                  <form class="form form-horizontal" method="post" action="{{ route('restaurants.holidays.store', $restaurantid) }}">
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"></h4>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_day">Operation Date</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Operation Date" id="ops_date" name="ops_date" required>
                           </div>
                        </div>
                        
                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_start">Operation Starts</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control ops_start" placeholder="Operation Starts" name="ops_start" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_end">Operation Ends</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control ops_end" placeholder="Operation Ends" name="ops_end" required>
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                           <i class="ft-save"></i> Add Operation Hours
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>

         <section class="card">
            <div class="card-header">
               <h4 class="card-title"><i class="ft-info"></i> Restaurant Holiday Operation Details</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a class="card-collapse" data-action="collapse"><i class="ft-minus"></i></a></li>
                   </ul>
               </div>
            </div>
            
            <div class="card-content collapse show" aria-expanded="true">
               <div class="card-body">
                  <div class="card-text"></div>

                  <form class="form form-horizontal">
                     <div class="form-body">
                        <h4 class="form-section"></h4>

                        <div class="form-group row">
                           <label class="col-md-2 label-control text-center" for="ops_date">Date</label>
                           <label class="col-md-2 label-control text-center" for="ops_start">Starts</label>
                           <label class="col-md-2 label-control text-center" for="ops_end">Ends</label>
                           <label class="col-md-2 label-control text-center" for="is_active">Status</label>
                        </div>

                        @empty($holiday)
                           <div class="alert alert-warning">No holiday date specified</div>
                        @else
                           @foreach($holiday as $value)
                              <div class="form-group row">
                                 <div class="col-md-2">
                                    <input type="hidden" value="{{ $value->id }}" required>
                                    <input type="text" class="form-control text-center" value="{{ $value->ops_date }}" readonly>
                                 </div>
                                 <div class="col-md-2">
                                    <input type="text" class="form-control text-center ops_start" value="{{ $value->ops_start }}" readonly>
                                 </div>
                                 <div class="col-md-2">
                                    <input type="text" class="form-control text-center ops_end" value="{{ $value->ops_end }}" readonly>
                                 </div>
                                 <div class="col-md-2 text-center">
                                    @if($value->is_active == 1)
                                       <div class="badge badge-success round">
                                          <span>Active</span>
                                          <i class="ft-check font-medium-2"></i>
                                       </div>
                                    @else
                                       <div class="badge badge-danger round">
                                          <span>In-Active</span>
                                          <i class="ft-x font-medium-2"></i>
                                       </div> 
                                    @endif
                                 </div>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                       <div class="btn-group" role="group">
                                          <!-- Button group with icons -->
                                          <form></form>
                                          <form id="updateholidayform_{{ $value->id }}" method="post" action="{{ route('restaurants.holidays.status', $restaurantid) }}">
                                             @method('patch')
                                             @csrf

                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <input type="hidden" name="is_active" value="{!! ($value->is_active == 1) ? 0 : 1; !!}">
                                             <button id="updateholiday_{{ $value->id }}" type="button" {!! ($value->is_active == 1) ? 'class="btn btn-danger"' : 'class="btn btn-success"'; !!} data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate Holiday Operation Details"><i class="fa fa-power-off"></i></button>
                                          </form>

                                          <form id="deleteholidayform_{{ $value->id }}" method="post" action="{{ route('restaurants.holidays.delete', $restaurantid) }}">
                                             @method('delete')
                                             @csrf

                                             <input type="hidden" name="id" value="{!! $value->id !!}">
                                             <button id="deleteholiday_{{ $value->id }}" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Holiday Operation Details"><i class="ft-x"></i></button>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           @endforeach
                        @endempty

                     </div>

                     <div class="form-actions">
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#ops_day').select2({
         placeholder: 'Select operation day',
         allowClear: true
      })

      $('#ops_date').datetimepicker({
        format: 'YYYY-MM-DD',
      })

      $('.ops_start').datetimepicker({
        format: 'hh:mm A',
        stepping: 15
      })

      $('.ops_end').datetimepicker({
        format: 'hh:mm A',
        stepping: 15
      })
   })

   $('[id^=update_]').click(function() {
      if (confirm('Update details status?')) {
         x = this.id
         y = x.replace('update_', 'updateform_')
         $('#'+y).submit()
      }
   })

   $('[id^=delete_]').click(function() {
      if (confirm('Delete this details?')) {
         x = this.id
         y = x.replace('delete_', 'deleteform_')
         $('#'+y).submit()
      }
   })

   $('[id^=updateholiday_]').click(function() {
      if (confirm('Update holiday details status?')) {
         x = this.id
         y = x.replace('updateholiday_', 'updateholidayform_')
         $('#'+y).submit()
      }
   })

   $('[id^=deleteholiday_]').click(function() {
      if (confirm('Delete this holiday details?')) {
         x = this.id
         y = x.replace('deleteholiday_', 'deleteholidayform_')
         $('#'+y).submit()
      }
   })
</script>
@endsection