@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">View Profile</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.staff.profile') }}">View Profile</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>
                     <form class="form form-horizontal">
                        <div class="form-body">
                           <h4 class="form-section"><i class="ft-user"></i> Account Details</h4>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="name">Name</label>
                              <div class="col-md-9">
                                 <input type="text" id="name" class="form-control" value="{{ $data->name }}" readonly>
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="email">Email</label>
                              <div class="col-md-9">
                                 <input type="email" id="email" class="form-control" value="{{ $data->email }}" readonly>
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="phone">Contact No.</label>
                              <div class="col-md-9">
                                 <input type="text" id="phone" class="form-control" value="{{ $data->phone }}" readonly>
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="role">Access Level</label>
                              <div class="col-md-9">
                                 <input type="text" id="role" class="form-control" value="{{ ucwords($data->restaurantstaff->role) }}" readonly>
                              </div>
                           </div>

                           <div class="form-group row">
                              <label class="col-md-3 label-control" for="role">Date of Birth</label>
                              <div class="col-md-9">
                                 <div class='input-group date'>
                                     <input type="text" class="form-control" id="dob" value="{{ $data->dob }}" readonly />
                                     <div class="input-group-append">
                                         <span class="input-group-text">
                                             <span class="fa fa-calendar"></span>
                                         </span>
                                     </div>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection