@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Update Restaurant Staff</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">{!! $restaurant->name !!}</li>
                     <li class="breadcrumb-item">Staff</li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.staff', $restaurant->id) }}">List of Staff</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">{!! $restaurant->name !!} Staff</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-hover table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center" width="1%">#</th>
                                 <th>Name</th>
                                 <th class="text-center" width="15%">Email</th>
                                 <th class="text-center" width="10%">Role</th>
                                 <th class="text-center" width="5%">Contact No.</th>
                                 <th class="text-center" width="10%">D.o.B</th>
                                 <th width="1%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                                 <tr>
                                    <td class="text-center">{!! ($key+1) !!}</td>
                                    <td>{!! $value->users->name !!}</td>                  
                                    <td class="text-center">{!! $value->users->email !!}</td>
                                    <td class="text-center"><?php echo ($value->role == 'superadmin') ? 'Super Admin' : ucwords($value->role); ?></td>
                                    <td class="text-center">{!! $value->users->phone !!}</td>
                                    <td class="text-center">{!! $value->users->dob !!}</td>
                                    <td class="text-center">
                                       @if(auth()->user()->id != $value->users->id)
                                          <div class="form-group">
                                             <div class="btn-group" role="group">
                                                <!-- Deactivate Staff -->
                                                <form id="statform_{!! $value->users->id !!}" class="form form-horizontal" method="post" action="{{ route('restaurants.staff.status', $restaurantid) }}">
                                                   @method('patch')
                                                   @csrf
                                                   <input type="hidden" name="user_id" value="{!! $value->users->id !!}">
                                                   <input type="hidden" name="is_active" value="1">
                                                   <button id="status_{!! $value->users->id !!}" class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate Staff">
                                                      <i class="fa fa-power-off"></i>
                                                   </button>
                                                </form>
                                             </div>
                                          </div>
                                       @endif
                                    </td>
                                 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>

                  @endif

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
         // 'order'       : [[ 6, "desc" ]],
      })

      $('#user_id').select2({
         placeholder: 'Select User',
         allowClear: true,
      });

      $('#role').select2({
         placeholder: 'Select Role',
         allowClear: true,
      })
   })
</script>
<script>
   $('[id^=reset_]').click(function(e) {
      e.preventDefault()
      if(confirm('Reset this user\'s password?')) {
         x = this.id
         y = x.replace('reset_', '')
         $('#resetform_'+y).submit()
      }
   })

   $('[id^=status_]').click(function(e) {
      e.preventDefault()
      if(confirm('Deactivate this user?')) {
         x = this.id
         y = x.replace('status_', '')
         $('#statform_'+y).submit()
      }
   })

   $('[id^=delete_]').click(function(e) {
      e.preventDefault()
      if(confirm('Remove this user?')) {
         x = this.id
         y = x.replace('delete_', '')
         $('#form_'+y).submit()
      }
   })
</script>
@endsection