@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Menu</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.menu', $restaurantid) }}">Menu</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Menu</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.menu.create', $restaurantid) }}" class="btn btn-primary">
                      <i class="ft-plus"></i> New Menu
                  </a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-hover table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center" width="1%">#</th>
                                 <th width="38%">Name</th>
                                 <th class="text-center" width="5%">Price</th>
                                 <th class="text-center" width="10%">Category</th>
                                 <th class="text-center" width="25%">Image</th>
                                 {{-- <th class="text-center" width="1%">Promotion</th>
                                 <th class="text-center" width="5%">Promotion <br> Price</th> --}}
                                 <th class="text-center" width="1%">Active</th>
                                 <th width="10%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                                 <tr>
                                    <td class="text-center">{{ ($key+1) }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td class="text-center">{{ number_format($value->price, 2) }}</td>
                                    <td class="text-center">
                                       @empty($value->category)
                                          <div class="alert alert-warning">No category selected</div>
                                       @else
                                          <div class="badge badge-dark round">
                                             <span>{{ $value->category }}</span>
                                             <i class="ft-check font-medium-2"></i>
                                          </div>
                                       @endempty
                                    </td>
                                    <td class="text-center">
                                       @empty($value->image)
                                          <div class="alert alert-warning">No Image</div>
                                       @else
                                          @if(strpos($value->image, 'http') === true)
                                             <div class="media-right" href="#">
                                                <img class="border border-1 rounded border-dark" src="{{ $value->image }}" style="max-width: 250px;" />
                                             </div>
                                          @else
                                             <div class="media-right" href="#">
                                                <img class="border border-1 rounded border-dark" src="{{ asset($value->image) }}" style="max-width: 250px;" />
                                             </div>
                                          @endif
                                       @endif
                                    </td>
                                    {{-- <td class="text-center">
                                       @if($value->is_promotion == 1)
                                          <div class="badge badge-success round">
                                             <i class="ft-check font-medium-2"></i>
                                          </div>
                                       @else
                                          <div class="badge badge-warning round">
                                             <i class="ft-x font-medium-2"></i>
                                          </div> 
                                       @endif
                                    </td>
                                    <td class="text-center">{{ number_format($value->promotion_price, 2) }}</td> --}}
                                    <td class="text-center">
                                       @if($value->is_active == 1)
                                          <div class="badge badge-success round">
                                             <i class="ft-check font-medium-2"></i>
                                          </div>
                                       @else
                                          <div class="badge badge-danger round">
                                             <i class="ft-x font-medium-2"></i>
                                          </div> 
                                       @endif
                                    </td>
                                    <td class="text-center">
                                       <div class="btn-group" role="group" aria-label="Options">
                                          <!-- View Menu Details -->
                                          <input type="hidden" id="restaurantid" value="{!! $restaurantid !!}">
                                          <button id="info_{{ $value->id }}" class="btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="View Menu Details"><i class="ft-search"></i></button>

                                          @if($role == 'superadmin' || $role == 'admin')
                                             <!-- Edit Menu Details -->
                                             <a href="{{ route('restaurants.menu.edit', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Menu Details">
                                                <i class="ft-edit"></i>
                                             </a>
                                          
                                             <!-- Delete Menu -->
                                             <form id="form_{{ $value->id }}" class="form form-horizontal" method="post" action="{{ route('restaurants.menu.delete', $restaurantid) }}">
                                                @method('delete')
                                                @csrf

                                                <input type="hidden" name="id" value="{{ $value->id }}">
                                                <button id="delete_{{ $value->id }}" class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu">
                                                   <i class="ft-x"></i>
                                                </button>
                                             </form>
                                          @endif
                                       </div>
                                    </td>
                                 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>

                  @endif

               </div>
            </div>
         </section>
      </div>
   </div>
</div>

<div class="form-group">
   <!-- Modal -->
   <div class="modal fade text-left" id="modalinfo" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header bg-info white">
               <h4 class="modal-title" id="ModalLabel"></h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="table-responsive">
                  <table id="table" class="table table-hover table-striped w-100">
                     <tr>
                        <th width="20%">Status</th>
                        <td id="status"></td>
                     </tr>
                     <tr>
                        <th width="20%">Name</th>
                        <td><input type="text" id="name" class="form-control" disabled></td>
                     </tr>
                     <tr>
                        <th width="20%">Price</th>
                        <td><input type="text" id="price" class="form-control" disabled></td>
                     </tr>
                     <tr>
                        <th width="20%">Description</th>
                        <td><textarea id="description" class="form-control" cols="10" rows="5" disabled></textarea></td>
                     </tr>
                     <tr>
                        <th width="20%">Ingredients</th>
                        <td id="ingredients"></td>
                     </tr>
                     <tr>
                        <th width="20%">Category</th>
                        <td><input type="text" id="category" class="form-control" disabled></td>
                     </tr>
                     <tr>
                        <th width="20%">Image</th>
                        <td id="image"></td>
                     </tr>
                     <tr>
                        <th width="20%">Promotion</th>
                        <td id="promotion"></td>
                     </tr>
                     <tr>
                        <th width="20%">Promotion Price</th>
                        <td><input type="text" id="promotion_price" class="form-control" disabled></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
      })
   })
   
   $('[id^=info_]').click(function(e) {
      e.preventDefault()

      x = this.id
      y = x.replace('info_', '')
      z = window.location.origin
      restaurantid = $('#restaurantid').val()
      
      $.ajax({
         url: z+'/restaurants/menu/'+restaurantid+'/info/'+y,
         method: "GET",
         success: function(data) {
            console.log(data);

            // Status
            if(data.is_active == 1) {
               stat = '<div class="alert alert-success">Active</div>'
            } else {
               stat = '<div class="alert alert-danger">In-Active</div>'
            }

            // Images
            if(data.image != null) {
               if(data.image.includes('http')) {
                  img = '<img class="border border-1 rounded border-dark" src="'+data.image+'" alt="..." style="width: 400px;" />'
               } else {
                  img = '<img class="border border-1 rounded border-dark" src="'+z+data.image+'" alt="..." style="width: 400px;" />'
               }
            } else {
               img = '<div class="alert alert-warning">No Image</div>'
            }

            // Promotions
            if(data.is_promotion == 1) {
               promo = '<div class="alert alert-success">Yes</div>'
            } else {
               promo = '<div class="alert alert-danger">No</div>'
            }

            // Ingredients
            if(data.ingredients.length > 0) {
               ingredients = ''
               $.each(data.ingredients, function( key, value ) {
                  ingredients += '<div class="row pl-1 pb-1">'
                  ingredients += '<input class="col-md-6 form-control" type="text" value="'+value.name+'" disabled>'
                  ingredients += '<input class="col-md-2 ml-1 form-control" type="text" value="'+value.quantity+'" disabled>'
                  ingredients += '</div>'
               })
            } else {
               ingredients = null
            }

            $('#ModalLabel').html(data.name)
            $('#status').html(stat)
            $('#name').val(data.name)
            $('#price').val(data.price)
            $('#description').val(data.description)
            $('#ingredients').append(ingredients)
            $('#category').val(data.category)
            $('#image').html(img)
            $('#promotion').html(promo)
            $('#promotion_price').val(data.promotion_price)

            // Display Modal
            $('#modalinfo').modal('show')

         },
         error: function(data) {
            // console.log(data);
         }
      })
   })
   
   $('[id^=delete_]').click(function(e) {
      e.preventDefault()
      if(confirm('Remove this menu?')) {
         x = this.id
         y = x.replace('delete_', '')
         $('#form_'+y).submit()
      }
   })
</script>
@endsection