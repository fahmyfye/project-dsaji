@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">New Menu</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.menu', $restaurantid) }}">Menu</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.menu.create', $restaurantid) }}">New Menu</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Create New Menu</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.menu', $restaurantid) }}" class="btn btn-warning"><i class="ft-chevron-left"></i> Cancel</a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  <div class="form-body">
                     <form name="save_form" class="form form-horizontal striped-rows" method="post" action="{{ route('restaurants.menu.store', $restaurantid) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_active">Enabled</label>
                           <div class="col-md-9">
                              <input type="checkbox" id="is_active" name="is_active" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" checked>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="price">Price</label>
                           <div class="col-md-9">
                              <input type="number" id="price" name="price" class="form-control" value="{{ old('price') }}" min="0" step="0.01" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="description">Description</label>
                           <div class="col-md-9">
                              <textarea id="description" name="description" class="form-control" cols="10" rows="5">{{ old('description') }}</textarea>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ingredients">Ingredients</label>
                           <div class="col-md-9 attribute-repeater">
                              <div data-repeater-list="ingredient_group">
                                 <div class="input-group mb-1" data-repeater-item>
                                    <select class="col-md-6 form-control ingredients" id="id" name="id" required data-validation-required-message="This field is required">
                                       <option></option>
                                       @foreach($ingredient as $value)
                                          <option value="{{ $value->id }}">
                                             <strong>{{ $value->name }}</strong>
                                          </option>
                                       @endforeach
                                    </select>
                                    <input class="col-md-2 ml-1 form-control" type="text" name="quantity" placeholder="Eg. 200gram" required data-validation-required-message="This field is required">
                                    <button type="button" class="btn btn-danger ml-1" data-repeater-delete><i class="ft-x"></i></button>
                                 </div>
                              </div>

                              <button id="addingredients" type="button" data-repeater-create class="btn btn-secondary">
                                 <i class="ft-plus"></i> Add new ingredient
                              </button>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="category">Category</label>
                           <div class="col-md-9">
                              <select class="form-control" id="category" name="category">
                                 <option></option>
                                 @foreach($category as $value)
                                    <option value="{{ $value->id }}" <?php echo (old('category') == $value->id) ? 'selected' : ''; ?>>
                                       <strong>{{ $value->name }}</strong>
                                    </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="image">Image</label>
                           <div class="col-md-8 custom-file ml-1">
                              <input type="file" class="custom-file-input" id="image" name="image">
                              <label class="custom-file-label" for="image">Choose Image File</label>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_promotion">Promotion</label>
                           <div class="col-md-9">
                              <input type="checkbox" id="is_promotion" name="is_promotion" data-toggle="toggle" data-offstyle="danger" data-onstyle="warning" data-off="No" data-on="Yes" <?php echo (old('is_promotion') == 'on') ? 'checked' : ''; ?>>
                           </div>
                        </div>

                        <div id="promotion_row" class="form-group row" style="<?php echo (old('is_promotion') == 'on') ? '' : 'display: none;'; ?>">
                           <label class="col-md-3 label-control" for="promotion_price">Promotion Price</label>
                           <div class="col-md-9">
                              <input type="text" id="promotion_price" name="promotion_price" class="form-control" value="{{ old('promotion_price', 0) }}">
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control"></label>
                           <div class="col-md-9">
                              <button type="submit" class="btn btn-primary"><i class="ft-save"></i> Save</button>
                           </div>
                        </div> 
                     </form>   
                  </div> 

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#category').select2({
         placeholder: 'Select A Category'
      })
   })

   $('.attribute-repeater').repeater({
      initEmpty: true,
      show: function () {
         $(this).slideDown();
         $('.ingredients').select2({
            placeholder: 'Tag Ingredients',
            allowClear: true
         })
      },
      hide: function(remove) {
         if (confirm('Remove this item?')) {
            $(this).slideUp(remove);
         }
      },
   })

   $('#is_promotion').change(function() {
      if($('#is_promotion').is(':checked')) {
         $('#promotion_row').slideDown()

      } else {
         $('#promotion_row').slideUp()
         $('#promotion_price').val(0)

      }
   })
</script>
@endsection