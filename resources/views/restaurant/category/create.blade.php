@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">New Category</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.category', $restaurantid) }}">Category</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.category.create', $restaurantid) }}">New Category</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Create New Category</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.category', $restaurantid) }}" class="btn btn-warning"><i class="ft-chevron-left"></i> Cancel</a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  <div class="form-body">
                     <form name="save_form" class="form form-horizontal striped-rows" method="post" action="{{ route('restaurants.category.store', $restaurantid) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="image">Image</label>
                           <div class="col-md-8 custom-file ml-1">
                              <input type="file" class="custom-file-input" id="image" name="image">
                              <label class="custom-file-label" for="image">Choose Image File</label>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="sorting">Sorting</label>
                           <div class="col-md-9">
                              <input type="number" name="sorting" class="form-control" value="{{ old('sorting') }}">
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control"></label>
                           <div class="col-md-9">
                              <button type="submit" class="btn btn-primary"><i class="ft-save"></i> Save</button>
                           </div>
                        </div> 
                     </form>   
                  </div> 

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection