@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Edit Category</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.category', $restaurantid) }}">Category</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.category', ['restaurantid' => $restaurantid, 'id' => $data->id]) }}">Edit Category</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                  </div>

                  <form class="form form-horizontal" method="post" action="{{ route('restaurants.category.update', $restaurantid) }}" enctype="multipart/form-data">
                     @method('patch')
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Restaurant Category Details</h4>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_active">Enabled</label>
                           <div class="col-md-9">
                              <input type="checkbox" id="is_active" name="is_active" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" <?php echo ($data->is_active == 1) ? 'checked' : ''; ?>>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="hidden" name="id" value="{{ $data->id }}" required>
                              <input type="text" class="form-control" placeholder="Category Name" name="name" value="{{ $data->name }}">
                           </div>
                        </div>

                        <div class="form-group row ml-1">
                           <label class="col-md-3 label-control" for="cimage">Image</label>
                           <div class="col-md-9">
                              <div class="row py-1">
                                 <img class="border border-1 rounded border-dark" src="{!! $data->image !!}" alt="..." style="width: 400px;" />
                              </div>
                              <div class="row custom-file">
                                 <input type="file" class="custom-file-input" name="image">
                                 <label class="custom-file-label" for="image">Replace Image File</label>
                              </div>
                              
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="sorting">Sorting</label>
                           <div class="col-md-9">
                              <input type="number" class="form-control" placeholder="Sorting Order" name="sorting" value="{{ $data->sorting }}">
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <a href="{!! url()->previous() !!}" class="btn btn-warning mr-1"><i class="ft-x"></i> Back</a>
                        <button type="submit" class="btn btn-primary">
                           <i class="fa fa-check-square-o"></i> Update
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection