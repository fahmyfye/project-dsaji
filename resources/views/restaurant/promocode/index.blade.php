@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Promo Code</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.promocode', $restaurantid) }}">Promo Code</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Promo Code</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.promocode.create', $restaurantid) }}" class="btn btn-primary">
                      <i class="ft-plus"></i> New Promo Code
                  </a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-hover table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center" width="1%">#</th>
                                 <th>Name</th>
                                 <th class="text-center" width="10%">Code</th>
                                 <th class="text-center" width="5%">Type</th>
                                 <th class="text-center" width="5%">Value</th>
                                 <th class="text-center" width="5%">Limited</th>
                                 <th class="text-center" width="5%">Allocation</th>
                                 <th class="text-center" width="5%">Redeemed</th>
                                 <th class="text-center" width="5%">Status</th>
                                 <th width="10%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                                 <tr>
                                    <td class="text-center">{{ ($key+1) }}</td>
                                    <td>{{ $value->name }}</td>                  
                                    <td class="text-center">{{ $value->code }}</td>
                                    <td class="text-center">{{ $value->type }}</td>
                                    <td class="text-center">{{ $value->value }}</td>
                                    <td class="text-center">
                                       @if($value->is_limited == 1)
                                          <div class="badge badge-warning round">
                                             <span>Limited Allocation</span>
                                             <i class="ft-alert-triangle font-medium-2"></i>
                                          </div>
                                       @else
                                          <div class="badge badge-danger round">
                                             <span>Unlimited Allocation</span>
                                             <i class="ft-alert-triangle font-medium-2"></i>
                                          </div> 
                                       @endif
                                    </td>
                                    <td class="text-center">{{ $value->allocation }}</td>
                                    <td class="text-center">{{ $value->redeemed }}</td>
                                    <td class="text-center">
                                       @if($value->is_active == 1)
                                          <div class="badge badge-success round">
                                             <span>Active</span>
                                             <i class="ft-check font-medium-2"></i>
                                          </div>
                                       @else
                                          <div class="badge badge-danger round">
                                             <span>In-Active</span>
                                             <i class="ft-x font-medium-2"></i>
                                          </div> 
                                       @endif
                                    </td>
                                    <td class="text-center">
                                       <div class="form-group">
                                          <div class="btn-group" role="group">
                                             <!-- Edit Promo Code Details -->
                                             <a href="{{ route('restaurants.promocode.edit', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Promo Code"><i class="ft-edit-3"></i></a>

                                             @if($userrole == 'superadmin')
                                                <!-- Delete Promo Code -->
                                                <form id="form_{{ $value->id }}" class="form form-horizontal" method="post" action="{{ route('restaurants.promocode.delete', $restaurantid) }}">
                                                   @method('delete')
                                                   @csrf
                                                   <input type="hidden" name="id" value="{{ $value->id }}">
                                                   <button id="delete_{{ $value->id }}" class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Promo Code">
                                                      <i class="ft-x"></i>
                                                   </button>
                                                </form>
                                             @endif
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>

                  @endif

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
      })
   })
</script>
<script>
   $('[id^=delete_]').click(function(e) {
      e.preventDefault()
      if(confirm('Remove this promo code?')) {
         x = this.id
         y = x.replace('delete_', '')
         $('#form_'+y).submit()
      }
   })
</script>
@endsection