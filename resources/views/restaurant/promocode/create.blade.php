@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">New Promo Code</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.promocode', $restaurantid) }}">Promo Code</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.promocode.create', $restaurantid) }}">New Promo Code</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Create New Promo Code</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.promocode', $restaurantid) }}" class="btn btn-warning"><i class="ft-chevron-left"></i> Cancel</a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  <div class="form-body">
                     <form name="save_form" class="form form-horizontal striped-rows" method="post" action="{{ route('restaurants.promocode.store', $restaurantid) }}">
                        @csrf

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_active">Enabled</label>
                           <div class="col-md-5 float-left">
                              <input type="checkbox" id="is_active" name="is_active" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" checked>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-5">
                              <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="code">Code <br><small>(No spacing)</small></label>
                           <div class="col-md-5">
                              <input type="text" id="code" name="code" class="form-control" value="{{ old('code') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="type">Type</label>
                           <div class="col-md-8">
                              <div id="type" class="skin skin-square">
                                 <fieldset>
                                    <input type="radio" name="type" id="type-amount" value="amount" checked>
                                    <label for="type-amount">Amount</label>
                                 </fieldset>
                                 <fieldset>
                                    <input type="radio" name="type" id="type-percentage" value="percentage">
                                    <label for="type-percentage">Percentage</label>
                                 </fieldset>
                              </div>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="value">Value</label>
                           <div class="col-md-5">
                              <input type="text" id="value" name="value" class="form-control" value="{{ old('value') }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_limited">Limited Allocation</label>
                           <div class="col-md-5 float-left">
                              <input type="checkbox" id="is_limited" name="is_limited" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="Unlimited" data-on="Limited">
                           </div>
                        </div>

                        <div id="allocated_row" class="form-group row" style="display: none;">
                           <label class="col-md-3 label-control" for="allocation">Total Allocation</label>
                           <div class="col-md-5">
                              <input type="text" id="allocation" name="allocation" class="form-control" value="{{ old('allocation') ? old('allocation') : 0 }}">
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control"></label>
                           <div class="col-md-5">
                              <button type="submit" class="btn btn-primary"><i class="ft-save"></i> Save</button>
                           </div>
                        </div> 
                     </form>   
                  </div> 

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $('#is_limited').change(function() {
      if($('#is_limited').is(':checked')) {
         $('#allocated_row').slideDown()

      } else {
         $('#allocated_row').slideUp()
         $('#allocated').val(0)

      }
   })
</script>
@endsection