@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Closing Petty Cash</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.pettycash', $restaurantid) }}">Petty Cash</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.pettycash.closing', ['restaurantid' => $restaurantid, 'id' => $data->id]) }}">Closing Petty Cash</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Closing Petty Cash</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.pettycash', $restaurantid) }}" class="btn btn-warning"><i class="ft-chevron-left"></i> Cancel</a>
               </div>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                  </div>
                              
                  <form class="form form-horizontal" method="post" action="{{ route('restaurants.pettycash.update', $restaurantid) }}">
                     @method('patch')
                     @csrf

                     <input type="hidden" name="id" value="{!! $data->id !!}">
                     <input type="hidden" name="type" value="closing">

                     <div class="form-body">
                        <div class="form-group row">
                           <div class="col-md-6">
                              <h4 class="form-section"><i class="fa fa-money"></i> Notes</h4>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr100">MYR 100</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr100" value="{{ old('myr100', $data->closing->myr100) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr50">MYR 50</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr50" value="{{ old('myr50', $data->closing->myr50) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr20">MYR 20</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr20" value="{{ old('myr20', $data->closing->myr20) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr10">MYR 10</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr10" value="{{ old('myr10', $data->closing->myr10) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr5">MYR 5</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr5" value="{{ old('myr5', $data->closing->myr5) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr1">MYR 1</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr1" value="{{ old('myr1', $data->closing->myr1) }}" min="0">
                                 </div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <h4 class="form-section"><i class="fa fa-money"></i> Coins</h4>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr050">MYR 0.50</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr050" value="{{ old('myr050', $data->closing->myr050) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr020">MYR 0.20</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr020" value="{{ old('myr020', $data->closing->myr020) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr010">MYR 0.10</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr010" value="{{ old('myr010', $data->closing->myr010) }}" min="0">
                                 </div>
                              </div>

                              <div class="form-group row">
                                 <label class="col-md-3 label-control text-right" for="myr005">MYR 0.05</label>
                                 <div class="col-md-9">
                                    <input type="number" class="form-control" name="myr005" value="{{ old('myr005', $data->closing->myr005) }}" min="0">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group row">
                           <div class="col-md-12">
                              <h4 class="form-section"><i class="ft-clipboard"></i> Remarks</h4>

                              <div class="form-group row">
                                 <label class="col-md-2 label-control text-right" for="remarks">Remarks</label>
                                 <div class="col-md-10">
                                    <textarea class="form-control" name="remarks" cols="10" rows="5">{{ old('remarks', $data->remarks) }}</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="form-actions">
                           <button type="submit" class="btn btn-primary">
                              <i class="fa fa-check-square-o"></i> Update
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection