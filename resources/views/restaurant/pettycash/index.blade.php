@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Petty Cash</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.pettycash', $restaurantid) }}">Petty Cash</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title">Petty Cash</h4>
               <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
               <div class="heading-elements">
                  <a href="{{ route('restaurants.pettycash.create', $restaurantid) }}" class="btn btn-primary">
                      <i class="ft-plus"></i> New Petty Cash
                  </a>
               </div>
            </div>

            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  @if(!empty($data))
                     <div class="table-responsive">
                        <table id="table" class="table table-hover table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th class="text-center">Date</th>
                                 <th class="text-center" width="23%">Cash</th>
                                 <th class="text-center" width="23%">Additional</th>
                                 <th class="text-center" width="23%">CLosing</th>
                                 <th width="10%"></th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($data as $key => $value)
                                 <tr>
                                    <td class="text-center">{!! $value->date !!}</td>
                                    <td>
                                       <div class="container mb-1">
                                          <div class="row justify-content-md-center">
                                             <div class="col-md-6 border-right-primary">
                                                <p><u>Notes</u></p>

                                                <div class="font-small-3">MYR 100 (x{!! $value->cash->myr100 !!})</div>
                                                <div class="font-small-3">MYR 50 (x{!! $value->cash->myr50 !!})</div>
                                                <div class="font-small-3">MYR 20 (x{!! $value->cash->myr20 !!})</div>
                                                <div class="font-small-3">MYR 10 (x{!! $value->cash->myr10 !!})</div>
                                                <div class="font-small-3">MYR 5 (x{!! $value->cash->myr5 !!})</div>
                                                <div class="font-small-3">MYR 1 (x{!! $value->cash->myr1 !!})</div>
                                             </div>
                                             <div class="col-md-6">
                                                <p><u>Coins</u></p>

                                                <div class="font-small-3">MYR 0.50 (x{!! $value->cash->myr050 !!})</div>
                                                <div class="font-small-3">MYR 0.20 (x{!! $value->cash->myr020 !!})</div>
                                                <div class="font-small-3">MYR 0.10 (x{!! $value->cash->myr010 !!})</div>
                                                <div class="font-small-3">MYR 0.05 (x{!! $value->cash->myr005 !!})</div>
                                             </div>
                                          </div>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="container mb-1">
                                          <div class="row justify-content-md-center">
                                             <div class="col-md-6 border-right-primary">
                                                <p><u>Notes</u></p>

                                                <div class="font-small-3">MYR 100 (x{!! $value->additional->myr100 !!})</div>
                                                <div class="font-small-3">MYR 50 (x{!! $value->additional->myr50 !!})</div>
                                                <div class="font-small-3">MYR 20 (x{!! $value->additional->myr20 !!})</div>
                                                <div class="font-small-3">MYR 10 (x{!! $value->additional->myr10 !!})</div>
                                                <div class="font-small-3">MYR 5 (x{!! $value->additional->myr5 !!})</div>
                                                <div class="font-small-3">MYR 1 (x{!! $value->additional->myr1 !!})</div>
                                             </div>
                                             <div class="col-md-6">
                                                <p><u>Coins</u></p>

                                                <div class="font-small-3">MYR 0.50 (x{!! $value->additional->myr050 !!})</div>
                                                <div class="font-small-3">MYR 0.20 (x{!! $value->additional->myr020 !!})</div>
                                                <div class="font-small-3">MYR 0.10 (x{!! $value->additional->myr010 !!})</div>
                                                <div class="font-small-3">MYR 0.05 (x{!! $value->additional->myr005 !!})</div>
                                             </div>
                                          </div>
                                       </div>
                                    </td>
                                    <td>
                                       <div class="container mb-1">
                                          <div class="row justify-content-md-center">
                                             <div class="col-md-6 border-right-primary">
                                                <p><u>Notes</u></p>

                                                <div class="font-small-3">MYR 100 (x{!! $value->closing->myr100 !!})</div>
                                                <div class="font-small-3">MYR 50 (x{!! $value->closing->myr50 !!})</div>
                                                <div class="font-small-3">MYR 20 (x{!! $value->closing->myr20 !!})</div>
                                                <div class="font-small-3">MYR 10 (x{!! $value->closing->myr10 !!})</div>
                                                <div class="font-small-3">MYR 5 (x{!! $value->closing->myr5 !!})</div>
                                                <div class="font-small-3">MYR 1 (x{!! $value->closing->myr1 !!})</div>
                                             </div>
                                             <div class="col-md-6">
                                                <p><u>Coins</u></p>

                                                <div class="font-small-3">MYR 0.50 (x{!! $value->closing->myr050 !!})</div>
                                                <div class="font-small-3">MYR 0.20 (x{!! $value->closing->myr020 !!})</div>
                                                <div class="font-small-3">MYR 0.10 (x{!! $value->closing->myr010 !!})</div>
                                                <div class="font-small-3">MYR 0.05 (x{!! $value->closing->myr005 !!})</div>
                                             </div>
                                          </div>
                                       </div>
                                    </td>
                                    <td class="text-center">
                                       <div class="btn-group" role="group" aria-label="Options">

                                          <!-- Edit Petty Cash Details -->
                                          <a href="{{ route('restaurants.pettycash.edit', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Petty Cash Details">
                                             <i class="ft-edit"></i>
                                          </a>

                                          <!-- Additional Petty Cash Details -->
                                          <a href="{{ route('restaurants.pettycash.additional', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Additional Petty Cash">
                                             <i class="ft-plus"></i>
                                          </a>

                                          <!-- Closing Petty Cash Details -->
                                          <a href="{{ route('restaurants.pettycash.closing', ['restaurantid' => $restaurantid, 'id' => $value->id]) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Closing Petty Cash">
                                             <i class="ft-minus"></i>
                                          </a>
                                       
                                          <!-- Delete Menu -->
                                          <form id="form_{{ $value->id }}" class="form form-horizontal" method="post" action="{{ route('restaurants.pettycash.delete', $restaurantid) }}">
                                             @method('delete')
                                             @csrf

                                             <input type="hidden" name="id" value="{{ $value->id }}">
                                             <button id="delete_{{ $value->id }}" class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Menu">
                                                <i class="ft-x"></i>
                                             </button>
                                          </form>
                                       </div>
                                    </td>
                                 </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>

                  @endif

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#table').DataTable({
         'scrollX'     : true,
         'paging'      : true,
         'lengthChange': true,
         'searching'   : true,
         'ordering'    : true,
         'info'        : true,
      })
   })
   
   $('[id^=delete_]').click(function(e) {
      e.preventDefault()
      if(confirm('Remove this item?')) {
         x = this.id
         y = x.replace('delete_', '')
         $('#form_'+y).submit()
      }
   })
</script>
@endsection