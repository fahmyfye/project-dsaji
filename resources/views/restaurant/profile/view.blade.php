@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Restaurant Profile</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item">Settings</li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.profile', $restaurantid) }}">Profile</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text"></div>

                  <form class="form form-horizontal">
                     <div class="form-body">
                        <h4 class="form-section"><i class="ft-info"></i> Restaurant Details</h4>
                        
                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_active">Enabled</label>
                           <div class="col-md-9">
                              <input type="checkbox" id="is_active" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" <?php echo ($data->is_active == 1) ? 'checked' : ''; ?> disabled>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="hidden" name="id" value="{{ $data->id }}" required>
                              <input type="text" class="form-control" value="{{ $data->name }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="email">Email</label>
                           <div class="col-md-9">
                              <input type="email" class="form-control" value="{{ $data->email }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="phone">Contact No.</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control" value="{{ $data->phone }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="address1">Address</label>
                           <div class="col-md-9">
                              <input type="text" id="address1" class="form-control" value="{{ $data->address1 }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->address2 }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->address3 }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->postcode }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->city }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->state }}" readonly>
                              <input type="text" class="form-control" value="{{ $data->country }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_start">Operation Starts</label>
                           <div class="col-md-9">
                              <input type="text" id="ops_start" class="form-control" value="{{ $data->ops_start }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_end">Operation Ends</label>
                           <div class="col-md-9">
                              <input type="text" id="ops_end" class="form-control" value="{{ $data->ops_end }}" readonly>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_day">Operation Days</label>
                           <div class="col-md-9">
                              <select class="form-control" id="ops_day">
                                 <option></option>
                                 @foreach($days as $day)
                                    <option value="{!! $day !!}" <?php echo (in_array($day, (array) json_decode($data->ops_day))) ? 'selected' : ''; ?>>
                                       <strong>+ {!! $day !!}</strong>
                                    </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_holiday">Operation During Holidays</label>
                           <div class="col-md-9">
                              <input type="checkbox" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" <?php echo ($data->ops_holiday == 1) ? 'checked' : ''; ?> disabled>
                           </div>
                        </div>

                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#ops_day').select2({
         placeholder: 'Select Operation Days',
         disabled: true
      })
   })
</script>
@endsection