@extends('layouts.master')

@section('content')
<div class="app-content content">
   <div class="content-wrapper">
      <div class="content-header row">
         <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Edit Restaurant</h3>
            <div class="row breadcrumbs-top">
               <div class="breadcrumb-wrapper col-12">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                     <li class="breadcrumb-item"><a href="{{ route('restaurants.index') }}">Restaurant</a></li>
                     <li class="breadcrumb-item active"><a href="{{ route('restaurants.edit', $data->id) }}">Edit Restaurant</a></li>
                  </ol>
               </div>
            </div>
         </div>
         <div class="content-header-right text-md-right col-md-6 col-12">
            <div class="form-group">
            </div>
         </div>
      </div>

      {{-- Session Flash Message --}}
      @if(Session::has('message'))
         <div class="alert {{ Session::get('alert-class')}} alert-dismissible mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message')}}</strong>
         </div>
      @endif

      {{-- Error Message --}}
      @if($errors->any())
         <div class="alert alert-danger alert-dismissible mb-2" role="alert">
            @foreach ($errors->all() as $error)
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <strong class="alert-danger">{{ $error }}</strong>
            @endforeach
         </div>
      @endif

      <div class="content-body">
         <section class="card">
            <div class="card-header">
               <h4 class="card-title"></h4>
            </div>
            <div class="card-content">
               <div class="card-body">
                  <div class="card-text">
                  </div>

                  <form class="form form-horizontal" method="post" action="{{ route('restaurants.update') }}">
                     @method('patch')
                     @csrf

                     <div class="form-body">
                        <h4 class="form-section"><i class="ft-user"></i> Restaurant Details</h4>

                        <!-- Status -->
                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="is_active">Enabled</label>
                           <div class="col-md-9">
                              <input type="checkbox" id="is_active" name="is_active" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" <?php echo ($data->is_active == 1) ? 'checked' : ''; ?>>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Name</label>
                           <div class="col-md-9">
                              <input type="hidden" name="id" value="{{ $data->id }}" required>
                              <input type="text" class="form-control" placeholder="Name" name="name" value="{{ $data->name }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="name">Nickname <br><small>Limit to 15 characters</small></label>
                           <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Nickname" name="nickname" value="{{ $data->nickname }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="email">Email</label>
                           <div class="col-md-9">
                              <input type="email" class="form-control" placeholder="Email" name="email" value="{{ $data->email }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="phone">Contact No.</label>
                           <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Contact No." name="phone" value="{{ $data->phone }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="password">Address</label>
                           <div class="col-md-9">
                              <input type="text" name="address1" class="form-control" placeholder="Address 1" value="{{ $data->address1 }}" required>
                              <input type="text" name="address2" class="form-control" placeholder="Address 2" value="{{ $data->address2 }}" required>
                              <input type="text" name="address3" class="form-control" placeholder="Address 3 (optional)" value="{{ $data->address3 }}">
                              <input type="text" name="postcode" class="form-control" placeholder="Postcode" value="{{ $data->postcode }}" required>
                              <input type="text" name="city" class="form-control" placeholder="City" value="{{ $data->city }}" required>
                              <input type="text" name="state" class="form-control" placeholder="State" value="{{ $data->state }}" required>
                              <input type="text" name="country" class="form-control" placeholder="Country" value="{{ $data->country }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_start">Operation Starts</label>
                           <div class="col-md-9">
                              <input type="text" id="ops_start" class="form-control" placeholder="Operation Starts" name="ops_start" value="{{ $data->ops_start }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_end">Operation Ends</label>
                           <div class="col-md-9">
                              <input type="text" id="ops_end" class="form-control" placeholder="Operation Ends" name="ops_end" value="{{ $data->ops_end }}" required>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_day">Operation Days</label>
                           <div class="col-md-9">
                              <select class="form-control" multiple="multiple" id="ops_day" name="ops_day[]">
                                 <option></option>
                                 @foreach($days as $day)
                                    <option value="{!! $day !!}" <?php echo (in_array($day, (array) json_decode($data->ops_day))) ? 'selected' : ''; ?>>
                                       <strong>+ {!! $day !!}</strong>
                                    </option>
                                 @endforeach
                              </select>
                           </div>
                        </div>

                        <div class="form-group row">
                           <label class="col-md-3 label-control" for="ops_holiday">Operation During Holidays</label>
                           <div class="col-md-9">
                              <input type="checkbox" name="ops_holiday" data-toggle="toggle" data-offstyle="danger" data-onstyle="success" data-off="No" data-on="Yes" <?php echo ($data->ops_holiday == 1) ? 'checked' : ''; ?>>
                           </div>
                        </div>

                     </div>

                     <div class="form-actions">
                        <a href="{!! url()->previous() !!}" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</a>
                        <button type="submit" class="btn btn-primary">
                           <i class="fa fa-check-square-o"></i> Save
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(window).on('load', function() {
      $('#ops_day').select2({
         placeholder: 'Select Operation Days',
         allowClear: true
      })

      $('#ops_start').datetimepicker({
        format: 'HH:mm:ss',
        stepping: 15
      })

      $('#ops_end').datetimepicker({
        format: 'HH:mm:ss',
        stepping: 15
      })
   })
</script>
@endsection