<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('auth.login');
});

Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth', 'access']], function() {
	// Dashboard
	Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

	// User Change Password
	Route::group(['prefix' => 'user'], function() {
		Route::get('/password', 'UserController@getpassword')->name('users.password');
		Route::patch('/password', 'UserController@password')->name('users.password.update');
	});

	// Restaurant Module
	Route::group(['prefix' => 'restaurants'], function() {
		Route::get('/dashboard', 'RestaurantController@dashboard')->name('restaurants.dashboard');

		Route::group(['prefix' => 'staff'], function() {
			Route::get('/profile', 'RestaurantStaffController@profile')->name('restaurants.staff.profile');

			Route::group(['prefix' => '{restaurantid}'], function() {				
				Route::get('/', 'RestaurantStaffController@index')->name('restaurants.staff');
				Route::post('/', 'RestaurantStaffController@store')->name('restaurants.staff.store');
				Route::patch('/status', 'RestaurantStaffController@status')->name('restaurants.staff.status');
				Route::get('/archived', 'RestaurantStaffController@archived')->name('restaurants.staff.archived');
			});
		});

		Route::group(['prefix' => 'profile/{restaurantid}'], function() {
			Route::get('/', 'RestaurantProfileController@index')->name('restaurants.profile');
		});

		Route::group(['prefix' => 'operations/{restaurantid}'], function() {
			Route::get('/', 'RestaurantOperationController@index')->name('restaurants.operations');
			Route::post('/', 'RestaurantOperationController@store')->name('restaurants.operations.store');
			Route::patch('/', 'RestaurantOperationController@status')->name('restaurants.operations.status');
			Route::delete('/', 'RestaurantOperationController@delete')->name('restaurants.operations.delete');
		});

		Route::group(['prefix' => 'holiday/{restaurantid}'], function() {
			Route::get('/', 'RestaurantHolidayController@index')->name('restaurants.holidays');
			Route::post('/', 'RestaurantHolidayController@store')->name('restaurants.holidays.store');
			Route::patch('/', 'RestaurantHolidayController@status')->name('restaurants.holidays.status');
			Route::delete('/', 'RestaurantHolidayController@delete')->name('restaurants.holidays.delete');
		});

		Route::group(['prefix' => 'pettycash/{restaurantid}'], function() {
			Route::get('/', 'RestaurantPettyCashController@index')->name('restaurants.pettycash');
			Route::get('/new', 'RestaurantPettyCashController@create')->name('restaurants.pettycash.create');
			Route::post('/', 'RestaurantPettyCashController@store')->name('restaurants.pettycash.store');
			Route::get('/edit/{id}', 'RestaurantPettyCashController@edit')->name('restaurants.pettycash.edit');
			Route::get('/additional/{id}', 'RestaurantPettyCashController@additional')->name('restaurants.pettycash.additional');
			Route::get('/closing/{id}', 'RestaurantPettyCashController@closing')->name('restaurants.pettycash.closing');
			Route::patch('/', 'RestaurantPettyCashController@update')->name('restaurants.pettycash.update');
			Route::delete('/', 'RestaurantPettyCashController@delete')->name('restaurants.pettycash.delete');
		});

		Route::group(['prefix' => 'ingredient/{restaurantid}'], function() {
			Route::get('/', 'RestaurantIngredientController@index')->name('restaurants.ingredient');
			Route::get('/new', 'RestaurantIngredientController@create')->name('restaurants.ingredient.create');
			Route::post('/', 'RestaurantIngredientController@store')->name('restaurants.ingredient.store');
			Route::get('/edit/{id}', 'RestaurantIngredientController@edit')->name('restaurants.ingredient.edit');
			Route::patch('/', 'RestaurantIngredientController@update')->name('restaurants.ingredient.update');
			Route::delete('/', 'RestaurantIngredientController@delete')->name('restaurants.ingredient.delete');
		});

		Route::group(['prefix' => 'category/{restaurantid}'], function() {
			Route::get('/', 'RestaurantCategoryController@index')->name('restaurants.category');
			Route::get('/new', 'RestaurantCategoryController@create')->name('restaurants.category.create');
			Route::post('/', 'RestaurantCategoryController@store')->name('restaurants.category.store');
			Route::get('/edit/{id}', 'RestaurantCategoryController@edit')->name('restaurants.category.edit');
			Route::patch('/', 'RestaurantCategoryController@update')->name('restaurants.category.update');
			Route::delete('/', 'RestaurantCategoryController@delete')->name('restaurants.category.delete');
		});

		Route::group(['prefix' => 'table/{restaurantid}'], function() {
			Route::get('/', 'RestaurantTableController@index')->name('restaurants.table');
			Route::get('/new', 'RestaurantTableController@create')->name('restaurants.table.create');
			Route::post('/', 'RestaurantTableController@store')->name('restaurants.table.store');
			Route::get('/edit/{id}', 'RestaurantTableController@edit')->name('restaurants.table.edit');
			Route::patch('/', 'RestaurantTableController@update')->name('restaurants.table.update');
			Route::delete('/', 'RestaurantTableController@delete')->name('restaurants.table.delete');
		});

		Route::group(['prefix' => 'promocode/{restaurantid}'], function() {
			Route::get('/', 'RestaurantPromoCodeController@index')->name('restaurants.promocode');
			Route::get('/new', 'RestaurantPromoCodeController@create')->name('restaurants.promocode.create');
			Route::post('/', 'RestaurantPromoCodeController@store')->name('restaurants.promocode.store');
			Route::get('/edit/{id}', 'RestaurantPromoCodeController@edit')->name('restaurants.promocode.edit');
			Route::patch('/', 'RestaurantPromoCodeController@update')->name('restaurants.promocode.update');
			Route::delete('/', 'RestaurantPromoCodeController@delete')->name('restaurants.promocode.delete');
		});

		Route::group(['prefix' => 'menu/{restaurantid}'], function() {
			Route::get('/', 'RestaurantMenuController@index')->name('restaurants.menu');
			Route::get('/new', 'RestaurantMenuController@create')->name('restaurants.menu.create');
			Route::post('/', 'RestaurantMenuController@store')->name('restaurants.menu.store');
			Route::get('/edit/{id}', 'RestaurantMenuController@edit')->name('restaurants.menu.edit');
			Route::patch('/', 'RestaurantMenuController@update')->name('restaurants.menu.update');
			Route::get('/info/{id}', 'RestaurantMenuController@info')->name('restaurants.menu.info');
			Route::delete('/', 'RestaurantMenuController@delete')->name('restaurants.menu.delete');
		});

		Route::group(['prefix' => 'bundle/{restaurantid}'], function() {
			Route::get('/', 'RestaurantBundleController@index')->name('restaurants.bundle');
			Route::get('/new', 'RestaurantBundleController@create')->name('restaurants.bundle.create');
			Route::post('/', 'RestaurantBundleController@store')->name('restaurants.bundle.store');
			Route::get('/edit/{id}', 'RestaurantBundleController@edit')->name('restaurants.bundle.edit');
			Route::patch('/', 'RestaurantBundleController@update')->name('restaurants.bundle.update');
			Route::get('/info/{id}', 'RestaurantBundleController@info')->name('restaurants.bundle.info');
			Route::delete('/', 'RestaurantBundleController@delete')->name('restaurants.bundle.delete');
		});

		Route::group(['prefix' => 'orders'], function() {
			Route::get('/', 'RestaurantBundleController@index')->name('restaurants.orders');
			// Route::get('/new', 'RestaurantBundleController@create')->name('restaurants.category.create');
			// Route::post('/', 'RestaurantBundleController@store')->name('restaurants.category.store');
			// Route::get('/edit/{id}', 'RestaurantBundleController@edit')->name('restaurants.category.edit');
			// Route::patch('/', 'RestaurantBundleController@update')->name('restaurants.category.update');
			// Route::delete('/', 'RestaurantBundleController@delete')->name('restaurants.category.delete');
		});
	});


	/*
	|--------------------------------------------------------------------------
	| Super Admin & Admin Functions
	|--------------------------------------------------------------------------
	*/

	Route::group(['middleware' => 'adminaccess'], function() {
		// User Module
		Route::group(['prefix' => 'user'], function() {
			Route::get('/', 'UserController@index')->name('users');
			Route::post('/search', 'UserController@search')->name('users.search');
			Route::get('/new', 'UserController@create')->name('users.create');
			Route::post('/', 'UserController@store')->name('users.store');
			Route::get('/edit/{id}', 'UserController@edit')->name('users.edit');
			Route::patch('/', 'UserController@update')->name('users.update');
			Route::delete('/', 'UserController@delete')->name('users.delete');
			Route::patch('/status', 'UserController@status')->name('users.status');
			Route::get('/archived', 'UserController@archived')->name('users.archived');
			Route::patch('/reset', 'UserController@reset')->name('users.reset');
			Route::get('/profile', 'UserController@profile')->name('users.profile');
		});

		// Restaurant Module
		Route::group(['prefix' => 'restaurants'], function() {
			Route::group(['prefix' => 'profile'], function() {
				Route::get('/', 'RestaurantController@index')->name('restaurants.index');
				Route::get('/new', 'RestaurantController@create')->name('restaurants.create');
				Route::post('/', 'RestaurantController@store')->name('restaurants.store');
				Route::get('/edit/{id}', 'RestaurantController@edit')->name('restaurants.edit');
				Route::patch('/', 'RestaurantController@update')->name('restaurants.update');
				Route::delete('/', 'RestaurantController@delete')->name('restaurants.delete');
				Route::patch('/status', 'RestaurantController@status')->name('restaurants.status');
				Route::get('/archived', 'RestaurantController@archived')->name('restaurants.archived');
			});
		});
	});
	
});
Route::get('/home', 'HomeController@index')->middleware('access')->name('home');
Route::get('/error', 'HomeController@error')->name('error');
