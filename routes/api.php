<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/'], function() {
	Route::group(['prefix' => 'user'], function() {
		Route::post('/', 'Api\CustomerController@login');
		Route::get('/', 'Api\CustomerController@info');
		Route::post('/update', 'Api\CustomerController@update');
		Route::post('/register', 'Api\CustomerController@register');
		Route::post('/logout', 'Api\CustomerController@logout');
		Route::post('/password', 'Api\CustomerController@password');
	});
});
