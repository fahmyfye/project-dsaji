<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurant_id');
            $table->string('name');
            $table->decimal('price', 8, 2)->default(0);
            $table->longText('description')->nullable();
            $table->longText('ingredients')->nullable();
            $table->integer('category')->nullable();
            $table->longText('image')->nullable();
            $table->tinyInteger('is_promotion')->default(0);
            $table->decimal('promotion_price', 8, 2)->default(0);
            $table->integer('sorting')->default(999);
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_menus');
    }
}
