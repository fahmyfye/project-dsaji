<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_bundles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurant_id');
            $table->longText('menus');
            $table->string('name');
            $table->decimal('price', 8, 2)->default(0);
            $table->longText('description')->nullable();
            $table->integer('category')->nullable();
            $table->longText('image')->nullable();
            $table->tinyInteger('is_promotion')->default(0);
            $table->decimal('promotion_price', 8, 2)->default(0);
            $table->tinyInteger('time_limited')->default(0);
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->integer('sorting')->default(999);
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_bundles');
    }
}
