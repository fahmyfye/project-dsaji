<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantSubCategoryTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_sub_categories')->insert([
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Sandwiches',
				'image'         => 'https://image.shutterstock.com/image-photo/marinated-grilled-healthy-chicken-breasts-600w-184074431.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Snacks',
				'image'         => 'https://image.shutterstock.com/image-photo/sirloin-steak-on-plate-600w-558645070.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Dumplings',
				'image'         => 'https://image.shutterstock.com/image-photo/sushi-set-sashimi-rolls-served-600w-366833861.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Confectionery',
				'image'         => 'https://image.shutterstock.com/image-photo/healthy-homemade-chickpea-veggies-salad-600w-535446121.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Pies',
				'image'         => 'https://image.shutterstock.com/image-photo/pouring-cola-drink-on-glass-600w-1354708346.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Salads',
				'image'         => 'https://image.shutterstock.com/image-photo/barman-hand-squeezing-fresh-juice-600w-1032633862.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Cereals',
				'image'         => 'https://image.shutterstock.com/image-photo/chocolate-brownie-cake-dessert-raspberries-600w-1568243872.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Fruits',
				'image'         => 'https://image.shutterstock.com/image-photo/creamy-roasted-red-bell-pepper-600w-1686897814.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Nuts',
				'image'         => 'https://image.shutterstock.com/image-photo/romantic-breakfast-setup-danish-pastries-600w-1310681567.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Malay Cuisines',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Chinese Cuisines',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Indian Cuisines',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Middle East Cuisines',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'category_id'   => rand(1, 10),
				'name'          => 'Japanese Cuisines',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => rand(1, 30),
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
