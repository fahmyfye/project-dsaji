<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantBundleTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_bundles')->insert([
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Gilled Chicken Wings',
				'price'         => '32.16',
				'description'   => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
				'category'      => 1,
				'image'         => 'https://image.shutterstock.com/image-photo/grilled-chicken-wings-barbecue-pepper-600w-1682082076.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Grilled Chicken Breast',
				'price'         => '27.58',
				'description'   => 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',
				'category'      => 1,
				'image'         => 'https://image.shutterstock.com/image-photo/grilled-chicken-breast-600w-622663148.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Chicken Rice with Scallions',
				'price'         => '16.47',
				'description'   => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
				'category'      => 1,
				'image'         => 'https://image.shutterstock.com/image-photo/general-chicken-rice-scallions-600w-1615685875.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Roasted Chicken Breast',
				'price'         => '16.78',
				'description'   => 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
				'category'      => 1,
				'image'         => 'https://image.shutterstock.com/image-photo/roasted-chicken-breast-mix-salad-600w-516703063.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Tomahawk Grilled Steak',
				'price'         => '15.27',
				'description'   => 'Fusce consequat. Nulla nisl. Nunc nisl.',
				'category'      => 2,
				'image'         => 'https://image.shutterstock.com/image-photo/hands-cut-grilled-tomahawk-meat-600w-1496804447.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Ribeye Steak with Pineapples',
				'price'         => '17.71',
				'description'   => 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
				'category'      => 2,
				'image'         => 'https://image.shutterstock.com/image-photo/ribeye-beef-steak-pineapple-dip-600w-1473838262.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Carrot Cake',
				'price'         => '18.42',
				'description'   => 'Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
				'category'      => 7,
				'image'         => 'https://image.shutterstock.com/image-photo/carrot-cake-walnuts-prunes-dried-600w-240942928.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Red Velvet',
				'price'         => '21.21',
				'description'   => 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
				'category'      => 7,
				'image'         => 'https://image.shutterstock.com/image-photo/red-velvet-cake-decorated-rose-600w-1256096239.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Plated Grilled Beef Steak',
				'price'         => '20.63',
				'description'   => 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.',
				'category'      => 2,
				'image'         => 'https://image.shutterstock.com/image-photo/plate-grilled-beef-steak-potatoes-600w-629070980.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Roast Beef Salad',
				'price'         => '23.30',
				'description'   => 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
				'category'      => 2,
				'image'         => 'https://image.shutterstock.com/image-photo/meat-salad-roast-beef-mushrooms-600w-613292273.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Chocolate Cake',
				'price'         => '24.24',
				'description'   => 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
				'category'      => 7,
				'image'         => 'https://image.shutterstock.com/image-photo/piece-chocolate-cake-mint-on-600w-347770127.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'menus'         => '[{"id":"1","quantity":"1"},{"id":"2","quantity":"2"},{"id":"5","quantity":"3"}]',
				'name'          => 'Bundle - Fruity Cream Puff',
				'price'         => '20.09',
				'description'   => 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
				'category'      => 7,
				'image'         => 'https://image.shutterstock.com/image-photo/delicious-puff-pastry-cream-fruits-600w-159317768.jpg',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
