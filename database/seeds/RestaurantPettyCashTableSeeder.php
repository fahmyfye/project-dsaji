<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantPettyCashTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_petty_cashes')->insert([
      	[
				'restaurant_id' => 1,
				'date'          => '2020-05-18',
				'cash'          => '{"myr100":"5","myr50":"10","myr20":"10","myr10":"10","myr5":"10","myr1":"10","myr050":"100","myr020":"100","myr010":"100","myr005":"50"}',
				'additional'    => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'closing'       => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'date'          => '2020-05-19',
				'cash'          => '{"myr100":"5","myr50":"10","myr20":"10","myr10":"10","myr5":"10","myr1":"10","myr050":"100","myr020":"100","myr010":"100","myr005":"50"}',
				'additional'    => '{"myr100":"0","myr50":"0","myr20":"10","myr10":"10","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'closing'       => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'date'          => '2020-05-20',
				'cash'          => '{"myr100":"5","myr50":"10","myr20":"10","myr10":"10","myr5":"10","myr1":"10","myr050":"100","myr020":"100","myr010":"100","myr005":"50"}',
				'additional'    => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'closing'       => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'date'          => '2020-05-21',
				'cash'          => '{"myr100":"5","myr50":"10","myr20":"10","myr10":"10","myr5":"10","myr1":"10","myr050":"100","myr020":"100","myr010":"100","myr005":"50"}',
				'additional'    => '{"myr100":"0","myr50":"5","myr20":"0","myr10":"0","myr5":"10","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'closing'       => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'date'          => '2020-05-22',
				'cash'          => '{"myr100":"5","myr50":"10","myr20":"10","myr10":"10","myr5":"10","myr1":"10","myr050":"100","myr020":"100","myr010":"100","myr005":"50"}',
				'additional'    => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'closing'       => '{"myr100":"0","myr50":"0","myr20":"0","myr10":"0","myr5":"0","myr1":"0","myr050":"0","myr020":"0","myr010":"0","myr005":"0"}',
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
