<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantTableTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_tables')->insert([
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 1',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 2',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 3',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 4',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 5',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 6',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 7',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 8',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 9',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 10',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 11',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 12',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 13',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 14',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 15',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 16',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 17',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 18',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 19',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Table 20',
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}
