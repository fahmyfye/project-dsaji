<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantStaffTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_staff')->insert([
      	[
				'user_id'       => 4,
				'restaurant_id' => 1,
				'role'          => 'superadmin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 4,
				'restaurant_id' => 2,
				'role'          => 'superadmin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 4,
				'restaurant_id' => 3,
				'role'          => 'superadmin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 4,
				'restaurant_id' => 4,
				'role'          => 'superadmin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 5,
				'restaurant_id' => 1,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 6,
				'restaurant_id' => 1,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 7,
				'restaurant_id' => 1,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 8,
				'restaurant_id' => 3,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
      	[
				'user_id'       => 9,
				'restaurant_id' => 4,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 10,
				'restaurant_id' => 7,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 11,
				'restaurant_id' => 4,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 12,
				'restaurant_id' => 5,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 13,
				'restaurant_id' => 11,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 14,
				'restaurant_id' => 10,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 15,
				'restaurant_id' => 6,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 16,
				'restaurant_id' => 5,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 17,
				'restaurant_id' => 1,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 18,
				'restaurant_id' => 7,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 19,
				'restaurant_id' => 7,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 20,
				'restaurant_id' => 1,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 21,
				'restaurant_id' => 4,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 22,
				'restaurant_id' => 1,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 23,
				'restaurant_id' => 5,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 24,
				'restaurant_id' => 1,
				'role'          => 'staff',
				'created_at'    => Carbon::now(),
	      ],
	      [
				'user_id'       => 25,
				'restaurant_id' => 10,
				'role'          => 'admin',
				'created_at'    => Carbon::now(),
			],

	   ]);
   }
}
