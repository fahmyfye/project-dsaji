<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
   /**
    * Seed the application's database.
    *
    * @return void
    */
   public function run()
   {
      $this->call(UsersTableSeeder::class);
      $this->call(RolesTableSeeder::class);
      $this->call(RestaurantTableSeeder::class);
      $this->call(RestaurantStaffTableSeeder::class);
      $this->call(RestaurantPettyCashTableSeeder::class);
      $this->call(RestaurantCategoryTableSeeder::class);
      $this->call(RestaurantSubCategoryTableSeeder::class);
      $this->call(RestaurantMenuTableSeeder::class);
      $this->call(RestaurantIngredientTableSeeder::class);
      $this->call(RestaurantTableTableSeeder::class);
      $this->call(RestaurantOperationTableSeeder::class);
      $this->call(RestaurantHolidayTableSeeder::class);
      $this->call(RestaurantBundleTableSeeder::class);
      $this->call(CustomerTableSeeder::class);
   }
}
