<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RestaurantCategoryTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('restaurant_categories')->insert([
      	[
				'restaurant_id' => 1,
				'name'          => 'Poultry',
				'image'         => 'https://image.shutterstock.com/image-photo/marinated-grilled-healthy-chicken-breasts-600w-184074431.jpg',
				'sorting'       => 14,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Steak',
				'image'         => 'https://image.shutterstock.com/image-photo/sirloin-steak-on-plate-600w-558645070.jpg',
				'sorting'       => 29,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Sushi',
				'image'         => 'https://image.shutterstock.com/image-photo/sushi-set-sashimi-rolls-served-600w-366833861.jpg',
				'sorting'       => 99,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Vegetarian',
				'image'         => 'https://image.shutterstock.com/image-photo/healthy-homemade-chickpea-veggies-salad-600w-535446121.jpg',
				'sorting'       => 44,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Soft Drinks',
				'image'         => 'https://image.shutterstock.com/image-photo/pouring-cola-drink-on-glass-600w-1354708346.jpg',
				'sorting'       => 42,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Cocktails',
				'image'         => 'https://image.shutterstock.com/image-photo/barman-hand-squeezing-fresh-juice-600w-1032633862.jpg',
				'sorting'       => 85,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Desserts',
				'image'         => 'https://image.shutterstock.com/image-photo/chocolate-brownie-cake-dessert-raspberries-600w-1568243872.jpg',
				'sorting'       => 95,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Soup',
				'image'         => 'https://image.shutterstock.com/image-photo/creamy-roasted-red-bell-pepper-600w-1686897814.jpg',
				'sorting'       => 58,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Pastry',
				'image'         => 'https://image.shutterstock.com/image-photo/romantic-breakfast-setup-danish-pastries-600w-1310681567.jpg',
				'sorting'       => 84,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
      	[
				'restaurant_id' => 1,
				'name'          => 'Finger Food',
				'image'         => 'https://image.shutterstock.com/image-photo/chicken-nacho-tortilla-cream-sauce-600w-1030356043.jpg',
				'sorting'       => 41,
				'is_active'     => 1,
				'created_at'    => Carbon::now(),
	      ],
	   ]);
   }
}