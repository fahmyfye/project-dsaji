<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('roles')->insert([
      	[
				'name'       => 'Super Admin',
				'slug'       => 'superadmin',
				'created_at' => Carbon::now(),
	      ],
      	[
				'name'       => 'Admin',
				'slug'       => 'admin',
				'created_at' => Carbon::now(),
	      ],
      	[
				'name'       => 'Restaurant',
				'slug'       => 'restaurant',
				'created_at' => Carbon::now(),
	      ],
    //   	[
				// 'name'       => 'Restaurant Admin',
				// 'slug'       => 'restaurantadmin',
				// 'created_at' => Carbon::now(),
	   //    ],
    //   	[
				// 'name'       => 'Restaurant Staff',
				// 'slug'       => 'restaurantstaff',
				// 'created_at' => Carbon::now(),
	   //    ],
	   ]);
   }
}
